package com.jason.aop;

import com.jason.aop.annotation.BehaviorTrace;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}
	
	@BehaviorTrace("摇一摇")
	public void mShake(View btn){
		//统计代码
		SystemClock.sleep(20);
		//结束
	}
	
	@BehaviorTrace("附近的人")
	public void mSayHello(View btn){
		SystemClock.sleep(10);
	}
	
	public void mScan(View btn){
		SystemClock.sleep(10);
	}
	
}
