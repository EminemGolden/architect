package com.jason.aop.aspect;

import java.lang.annotation.Annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import com.jason.aop.annotation.BehaviorTrace;

import android.util.Log;

/**
 * 用户行为统计的切面
 * @author Administrator
 *
 */
@Aspect
public class BehaviorAspect {

	//定义切面的规则
	//1.通俗来讲就是切面长什么样？哪些方法在这个切面上，哪些功能需要被统计
	//2.切面上方法执行，需要进行什么样的处理
	
	//只要带有BehaviorTrace注解的方法，都属于我这个切面
	@Pointcut("execution(@com.jason.aop.annotation.BehaviorTrace * *(..))")
	public void methodAnnotatedWithBehaviorTrace(){}
	
	/**
	 * *.new(..)  包含这个注解的所有实例化方法
	 */
	@Pointcut("execution(@com.jason.aop.annotation.BehaviorTrace *.new(..))")
	public void constructorAnnotatedWithBehaviorTrace(){}
	
	//环绕
	@Around("methodAnnotatedWithBehaviorTrace() || constructorAnnotatedWithBehaviorTrace()")
	public Object weaveJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable{
		//joinPoint 切点，面由点构成    获取方法签名  相当于获取方法
		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
		//这个切点是哪个类的哪个方法
		String className = methodSignature.getDeclaringType().getSimpleName();
		String methodName = methodSignature.getName();
		BehaviorTrace behaviorTrace = (BehaviorTrace)methodSignature.getMethod().getAnnotation(BehaviorTrace.class);
		String funName = behaviorTrace.value();
		//sprintf
		//统计执行耗时时间
		long begin = System.currentTimeMillis();
		//执行操作运算
		Object result = joinPoint.proceed();
		long duration = System.currentTimeMillis() - begin;
		Log.d("jason", String.format("功能：%s，%s 的  %s 方法执行，耗时：%d ms",funName,className,methodName,duration));
		return result;
	}
	
}
