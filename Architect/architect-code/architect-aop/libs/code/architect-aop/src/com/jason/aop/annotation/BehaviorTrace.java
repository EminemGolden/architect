package com.jason.aop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//存在于什么时期
//CLASS存在于class中
@Retention(RetentionPolicy.RUNTIME)
//使用在普通方法和构造方法上
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
/**
 * 标志该行为需要被统计
 * @author Administrator
 *
 */
public @interface BehaviorTrace {

	String value();
	
}
