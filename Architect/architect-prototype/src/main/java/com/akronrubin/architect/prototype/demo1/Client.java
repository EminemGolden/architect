package com.akronrubin.architect.prototype.demo1;

import org.junit.Test;

public class Client {

	@Test
	public void main() {
		// 个人订单对象
		PersonalOrder po = new PersonalOrder();
		po.setOrderProductNum(2999);
		po.setCustomerName("琅琊榜-胡歌");
		po.setProductId("P0002");

		// 企业订单
		EnterpriseOrder eo = new EnterpriseOrder();
		eo.setOrderProductNum(3999);
		eo.setEnterpriseName("华为");
		eo.setProductId("P0008");

		// VIP客户
		VIPOrder vo = new VIPOrder();
		vo.setOrderProductNum(3999);
		vo.setCustomerName("VIP01");
		vo.setProductId("P0008");

		OrderBiz biz = new OrderBiz();
		biz.saveOrder(vo);
	}
	
}
