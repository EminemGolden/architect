package com.akronrubin.architect.prototype.ordersystem;

/**
 * 处理业务拆分的业务类
 * 
 * @author EminemGolden
 *
 */
public class OrderBiz {

	public static final int MAX_ORDER_NUM = 1000;

	public void saveOrder(IOrder order) {
		if (null == order) {
			return;
		}
		System.out.println("拆分前订单：" + order);
		IOrder newOrder = null;
		PersonalOrder personalOrderTemp = null;
		EnterpriseOrder enterpriseOrderTemp = null;
		while (order.getOrderProductNum() > MAX_ORDER_NUM) {
			if (order instanceof PersonalOrder) {
				PersonalOrder personalOrder = (PersonalOrder) order;
				personalOrderTemp = new PersonalOrder();
				personalOrderTemp.setCustomerName(personalOrder.getCustomerName());
				personalOrderTemp.setProductId(personalOrder.getProductId());
				newOrder = personalOrderTemp;
			} else if (order instanceof EnterpriseOrder) {
				EnterpriseOrder enterpriseOrder = (EnterpriseOrder) order;
				enterpriseOrderTemp = new EnterpriseOrder();
				enterpriseOrderTemp.setEnterpriseName(enterpriseOrder.getEnterpriseName());
				enterpriseOrderTemp.setProductId(enterpriseOrder.getProductId());
				newOrder = enterpriseOrderTemp;
			}
			order.setOrderProductNum(order.getOrderProductNum() - MAX_ORDER_NUM);
			newOrder.setOrderProductNum(MAX_ORDER_NUM);
			System.out.println("拆分订单：" + newOrder);
			
			//处理拆分出来的订单业务逻辑
			
		}
		System.out.println("拆分完成订单："+order);
	}

}
