package com.akronrubin.architect.prototype.base;


public class TestClient {

    @org.junit.Test
    public void main() {
        IPrototype prototype = new ConcreatePrototypeImpl();
        System.out.println(prototype);
        Client client = new Client(prototype);
        System.out.println(client.operation());
    }
}
