package com.akronrubin.architect.prototype.demo1;

/**
 * 企业订单
 * 
 * @author EminemGolden
 *
 */
public class EnterpriseOrder implements IOrder {

	// 企业名称 J2EE
	private String enterpriseName;
	// 产品编号
	private String productId;
	// 产品数量
	private int orderProductNum;

	@Override
	public IOrder cloneOrder() {
		EnterpriseOrder order = new EnterpriseOrder();
		order.setEnterpriseName(this.getEnterpriseName());
		order.setOrderProductNum(this.getOrderProductNum());
		order.setProductId(this.getProductId());
		return order;
	}

	@Override
	public int getOrderProductNum() {
		return orderProductNum;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public void setOrderProductNum(int orderProductNum) {
		this.orderProductNum = orderProductNum;
	}

	@Override
	public String toString() {
		return "企业订单 [enterpriseName=" + enterpriseName + ", productId=" + productId + ", orderProductNum="
				+ orderProductNum + "]";
	}
}
