package com.akronrubin.architect.prototype.base;

/**
 * 原型模式具体实现类
 * 
 * @author EminemGolden
 *
 */
public class ConcreatePrototypeImpl implements IPrototype {

	@Override
	public IPrototype cloneMe() {
		return new ConcreatePrototypeImpl();
	}

}
