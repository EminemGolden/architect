package com.akronrubin.architect.prototype.demo2;

/**
 * 个人订单
 * 
 * @author EminemGolden
 *
 */
public class PersonalOrder implements IOrder {

	// 客户名称
	private String customerName;
	// 产品编号
	private Product product;
	// 订单数量
	private int orderProductNum;

	@Override
	public IOrder cloneOrder() {
		PersonalOrder order = new PersonalOrder();
		order.setCustomerName(this.getCustomerName());
		order.setOrderProductNum(this.getOrderProductNum());
		try {
			order.setProduct((Product) this.getProduct().clone());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return order;
	}

	
	@Override
	public int getOrderProductNum() {
		return orderProductNum;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	public void setOrderProductNum(int orderProductNum) {
		this.orderProductNum = orderProductNum;
	}

	@Override
	public String toString() {
		return "个人订单 [customerName=" + customerName + ", product="+ product +product.hashCode()+", orderProductNum="
				+ orderProductNum + "]";
	}

}
