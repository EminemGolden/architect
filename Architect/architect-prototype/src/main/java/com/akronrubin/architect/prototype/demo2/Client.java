package com.akronrubin.architect.prototype.demo2;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		// 个人订单对象
		PersonalOrder po = new PersonalOrder();
		po.setOrderProductNum(2999);
		po.setCustomerName("琅琊榜-胡歌");
		Product product = new Product();
		product.setName("充电宝");
		product.setProductId("PO210");
		po.setProduct(product);

		// 企业订单
		EnterpriseOrder eo = new EnterpriseOrder();
		eo.setOrderProductNum(3999);
		eo.setEnterpriseName("华为");
		Product product2 = new Product();
		product2.setName("充电宝");
		product2.setProductId("PO210");
		eo.setProduct(product2);


		OrderBiz biz = new OrderBiz();
		biz.saveOrder(po);
	}
	
}
