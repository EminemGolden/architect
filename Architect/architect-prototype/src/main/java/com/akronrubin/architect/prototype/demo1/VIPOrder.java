package com.akronrubin.architect.prototype.demo1;

public class VIPOrder implements IOrder {
	// 客户名称
	private String customerName;
	// 产品编号
	private String productId;
	// 订单数量
	private int orderProductNum;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public int getOrderProductNum() {
		return orderProductNum;
	}

	@Override
	public void setOrderProductNum(int num) {
		this.orderProductNum = num;
	}

	@Override
	public IOrder cloneOrder() {
		VIPOrder order = new VIPOrder();
		order.setOrderProductNum(this.getOrderProductNum());
		order.setCustomerName(this.getCustomerName());
		order.setProductId(this.getProductId());
		return order;
	}

	@Override
	public String toString() {
		return "VIP客户 [customerName=" + customerName + ", productId=" + productId + ", orderProductNum="
				+ orderProductNum + "]";
	}

}
