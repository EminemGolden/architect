package com.akronrubin.architect.prototype.base;

/**
 * 原型模式接口
 * 
 * @author EminemGolden
 *
 */
public interface IPrototype {

	IPrototype cloneMe();

}
