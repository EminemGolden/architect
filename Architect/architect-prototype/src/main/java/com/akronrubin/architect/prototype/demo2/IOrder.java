package com.akronrubin.architect.prototype.demo2;

/**
 * 订单接口
 * 
 * @author EminemGolden
 *
 */
public interface IOrder {
	/**
	 * 获取订单产品的数量
	 * 
	 * @return
	 */
	int getOrderProductNum();

	void setOrderProductNum(int num);

	IOrder cloneOrder();
}
