package com.akronrubin.architect.prototype.demo2;

/**
 * 企业订单
 * 
 * @author EminemGolden
 *
 */
public class EnterpriseOrder implements IOrder {

	// 企业名称 J2EE
	private String enterpriseName;
	// 产品编号
	private Product product;
	// 产品数量
	private int orderProductNum;

	@Override
	public IOrder cloneOrder() {
		EnterpriseOrder order = new EnterpriseOrder();
		order.setEnterpriseName(this.getEnterpriseName());
		order.setOrderProductNum(this.getOrderProductNum());
		try {
			order.setProduct((Product) this.getProduct().clone());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return order;
	}

	@Override
	public int getOrderProductNum() {
		return orderProductNum;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setOrderProductNum(int orderProductNum) {
		this.orderProductNum = orderProductNum;
	}

	@Override
	public String toString() {
		return "企业订单 [enterpriseName=" + enterpriseName + ", product=" + product +product.hashCode()+", orderProductNum="
				+ orderProductNum + "]";
	}
}
