package com.akronrubin.architect.prototype.base;

/**
 * 需要用到实现了克隆接口的类
 * 
 * @author EminemGolden
 *
 */
public class Client {

	IPrototype prototype;

	public Client(IPrototype prototype) {
		super();
		this.prototype = prototype;
	}

	public IPrototype operation() {
		if (null == this.prototype) {
			return null;
		}
		return this.prototype.cloneMe();
	}

}
