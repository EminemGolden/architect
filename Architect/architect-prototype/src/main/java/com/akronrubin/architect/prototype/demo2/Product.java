package com.akronrubin.architect.prototype.demo2;

public class Product implements Cloneable {

	// 产品名称
	private String name;
	// 产品编号
	private String productId;

	//
	// Maker maker;

	@Override
	protected Object clone() throws CloneNotSupportedException {
		//Product product = new Product();
		//product.setMaker((Maker)marker.clone());
		
		return super.clone();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", productId=" + productId + "]";
	}
	
	
}
