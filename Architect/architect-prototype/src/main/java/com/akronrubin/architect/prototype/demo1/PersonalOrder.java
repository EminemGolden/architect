package com.akronrubin.architect.prototype.demo1;

/**
 * 个人订单
 * 
 * @author EminemGolden
 *
 */
public class PersonalOrder implements IOrder {

	// 客户名称
	private String customerName;
	// 产品编号
	private String productId;
	// 订单数量
	private int orderProductNum;

	@Override
	public IOrder cloneOrder() {
		PersonalOrder order = new PersonalOrder();
		order.setCustomerName(this.getCustomerName());
		order.setOrderProductNum(this.getOrderProductNum());
		order.setProductId(this.getProductId());
		return order;
	}

	
	@Override
	public int getOrderProductNum() {
		return orderProductNum;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public void setOrderProductNum(int orderProductNum) {
		this.orderProductNum = orderProductNum;
	}

	@Override
	public String toString() {
		return "个人订单 [customerName=" + customerName + ", productId=" + productId + ", orderProductNum="
				+ orderProductNum + "]";
	}

}
