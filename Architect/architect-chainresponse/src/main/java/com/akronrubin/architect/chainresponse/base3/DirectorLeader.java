package com.akronrubin.architect.chainresponse.base3;
/**
 * 主管
 * @author wangdenghui
 *
 */
public class DirectorLeader extends Leader {

	public static final int DIRECTOR_LEADER_MAX = 5000;

	@Override
	protected int getLimite() {
		return DIRECTOR_LEADER_MAX;
	}

	@Override
	protected void handler(int money) {
		System.out.println(DirectorLeader.class.getSimpleName() + "处理了" + money + "元的报销账单...");
	}

}
