package com.akronrubin.architect.chainresponse.base3;

public class GroupLeader extends Leader {

	public static final int GROUP_LEADER_MAX = 1000;

	@Override
	protected int getLimite() {
		return GROUP_LEADER_MAX;
	}

	@Override
	protected void handler(int money) {
		System.out.println(GroupLeader.class.getSimpleName() + "处理了" + money + "元的报销账单...");
	}

}
