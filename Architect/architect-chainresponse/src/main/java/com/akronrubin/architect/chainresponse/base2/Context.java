package com.akronrubin.architect.chainresponse.base2;

public class Context {

	private static final Context INSTANCE = new Context();

	private Context() {
	}

	public static final Context getInstance() {
		return INSTANCE;
	}
	
	public void sendRequest(AbstractRequest request){
		AbstractHandler handler1 = new ConcreateHandler1();
		AbstractHandler handler2 = new ConcreateHandler2();
		AbstractHandler handler3 = new ConcreateHandler3();
		handler1.setNextHandler(handler2);
		handler2.setNextHandler(handler3);
		handler1.handleRequest(request);
	}

}
