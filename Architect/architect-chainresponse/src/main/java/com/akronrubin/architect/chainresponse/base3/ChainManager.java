package com.akronrubin.architect.chainresponse.base3;

public class ChainManager {

	public ChainManager() {
		super();
	}

	private static final ChainManager INSTANCE = new ChainManager();

	public static final ChainManager getInstance() {
		return INSTANCE;
	}

	public final void sendRequest(int money) {
		GroupLeader groupLeader = new GroupLeader();
		DirectorLeader directorLeader = new DirectorLeader();
		ManagerLeader managerLeader = new ManagerLeader();
		Boss boss = new Boss();
		groupLeader.setNextHandler(directorLeader);
		directorLeader.setNextHandler(managerLeader);
		managerLeader.setNextHandler(boss);
		groupLeader.handlerRequest(money);
	}

}
