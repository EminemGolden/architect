package com.akronrubin.architect.chainresponse.base;

/**
 * 抽象处理者
 * 
 * @author wangdenghui
 *
 */
public abstract class Handler {

	protected Handler handler;

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	/**
	 * /** 处理请求
	 * 
	 * @param condition
	 */
	public abstract boolean handleRequest(String condition);

}
