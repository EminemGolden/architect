package com.akronrubin.architect.chainresponse.base2;

/**
 * 具体消息处理类
 * 
 * @author wangdenghui
 *
 */
public abstract class AbstractHandler {

	/**
	 * 如果消息处理不了，下一个接受消息的传递对象
	 */
	private AbstractHandler nextHandler;

	/**
	 * 父类定义处理逻辑的模版方法
	 * 
	 * @param request
	 */
	public final void handleRequest(AbstractRequest request) {
		if (getRequestLevel() == request.getRequestLevel()) {
			handle(request);
		} else {
			nextHandler.handleRequest(request);
		}
	}

	
	public void setNextHandler(AbstractHandler nextHandler) {
		this.nextHandler = nextHandler;
	}
	
	/**
	 * 子类处理业务逻辑
	 * 
	 * @param request
	 */
	protected abstract void handle(AbstractRequest request);

	/**
	 * get handler do request level
	 * 
	 * @return
	 */
	protected abstract int getRequestLevel();

}
