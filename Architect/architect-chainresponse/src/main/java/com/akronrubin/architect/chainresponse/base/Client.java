package com.akronrubin.architect.chainresponse.base;

import org.junit.Test;

public class Client {
	
	@Test
	public void main() {
		Handler handler1 = new ConcreateHandlerA();
		Handler handler2 = new ConcreateHandlerB();
		handler1.setHandler(handler2);
		handler1.handleRequest(ConcreateHandlerB.MESSAGE_B);
		System.out.println("");
	}

}
