package com.akronrubin.architect.chainresponse.base2;

/**
 * 消息处理类的具体实现子类
 * 
 * @author wangdenghui
 *
 */
public class ConcreateHandler2 extends AbstractHandler {

	@Override
	protected void handle(AbstractRequest request) {
		System.out.println(this.getClass().getSimpleName() + "接收到对应的请求消息，执行操作结果...");
	}

	@Override
	protected int getRequestLevel() {
		return ConcreateRequest2.REQUEST2_LEVEL;
	}

}
