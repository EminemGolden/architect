package com.akronrubin.architect.chainresponse.base3;
/**
 * 主管
 * @author wangdenghui
 *
 */
public class Boss extends Leader {

	public static final int BOSS_LEADER_MAX = Integer.MAX_VALUE;

	@Override
	protected int getLimite() {
		return BOSS_LEADER_MAX;
	}

	@Override
	protected void handler(int money) {
		System.out.println(Boss.class.getSimpleName() + "处理了" + money + "元的报销账单...");
	}

}
