package com.akronrubin.architect.chainresponse.base3;
/**
 * 主管
 * @author wangdenghui
 *
 */
public class ManagerLeader extends Leader {

	public static final int MANAGER_LEADER_MAX = 10000;

	@Override
	protected int getLimite() {
		return MANAGER_LEADER_MAX;
	}

	@Override
	protected void handler(int money) {
		System.out.println(ManagerLeader.class.getSimpleName() + "处理了" + money + "元的报销账单...");
	}

}
