package com.akronrubin.architect.chainresponse.base;

/**
 * 具体的处理者
 * 
 * @author wangdenghui
 *
 */
public class ConcreateHandlerA extends Handler {

	public static final String MESSAGE_A = "A";

	@Override
	public boolean handleRequest(String condition) {
		if (condition.equals(MESSAGE_A)) {
			System.out.println("业务属于" + ConcreateHandlerA.class.getSimpleName() + "能力范围内,本类处理");
			return true;
		} else {
			System.out.println("业务不属于" + ConcreateHandlerA.class.getSimpleName() + "能力范围内,移交给下一个事件处理对象...");
			if (null != handler) {
				handler.handleRequest(condition);
			}
			return true;
		}
	}

}
