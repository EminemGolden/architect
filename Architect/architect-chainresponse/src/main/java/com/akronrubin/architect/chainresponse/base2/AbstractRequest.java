package com.akronrubin.architect.chainresponse.base2;

public class AbstractRequest {

	public static final int REQUEST3_LEVEL = 3;
	public static final int REQUEST1_LEVEL = 1;
	public static final int REQUEST2_LEVEL = 2;

	private Object data;
	private int level;

	public AbstractRequest(Object data, int level) {
		super();
		this.data = data;
		this.level = level;
	}

	public int getRequestLevel() {
		return level;
	}

	public Object getData() {
		return data;
	}

}
