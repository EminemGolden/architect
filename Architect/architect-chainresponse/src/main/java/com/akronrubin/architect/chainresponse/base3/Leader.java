package com.akronrubin.architect.chainresponse.base3;

public abstract class Leader {

	private Leader nextHandler;

	protected final void handlerRequest(int money) {
		if (getLimite() > money) {
			handler(money);
		} else {
			nextHandler.handlerRequest(money);
		}
	}
	
	public void setNextHandler(Leader nextHandler) {
		this.nextHandler = nextHandler;
	}
	protected abstract int getLimite();

	protected abstract void handler(int money);

}
