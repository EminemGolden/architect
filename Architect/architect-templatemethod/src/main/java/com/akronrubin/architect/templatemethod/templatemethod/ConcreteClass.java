package com.akronrubin.architect.templatemethod.templatemethod;

public class ConcreteClass extends AbstractClass {

	@Override
	public void doPrimitiveOperation1() {
		System.out.println("具体实现1");
	}

	@Override
	public void doPrimitiveOperation2() {
		System.out.println("具体实现2");
	}

}
