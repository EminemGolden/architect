package com.akronrubin.architect.templatemethod.templatemethod2;

/**
 * 军用电脑
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月6日
 * @version 1.0
 */
public class MilitaryComputer extends AbstractComputer {

	@Override
	protected void checkHardware() {
		System.out.println("检查硬件防火墙");
	}
	
	@Override
	protected void login() {
		System.out.println("指纹识别登录");
	}
	
}
