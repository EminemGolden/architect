package com.akronrubin.architect.templatemethod.templatemethod2;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		//程序员电脑开机
		AbstractComputer computer = new CoderComputer();
		computer.startup();
		
		//军用电脑
		computer = new MilitaryComputer();
		computer.startup();
	}

}
