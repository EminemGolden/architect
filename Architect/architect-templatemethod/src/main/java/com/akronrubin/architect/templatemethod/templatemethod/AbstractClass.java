package com.akronrubin.architect.templatemethod.templatemethod;

public abstract class AbstractClass {

	/**
	 * 原语操作1
	 */
	public abstract void doPrimitiveOperation1();

	/**
	 * 原语操作2
	 */
	public abstract void doPrimitiveOperation2();
	
	/**
	 * 模板方法，定义算法的结构
	 */
	public final void templateMethod(){
		doPrimitiveOperation1();
		doPrimitiveOperation2();
	}

}
