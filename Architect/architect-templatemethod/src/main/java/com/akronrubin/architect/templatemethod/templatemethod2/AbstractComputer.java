package com.akronrubin.architect.templatemethod.templatemethod2;

public abstract class AbstractComputer {

	protected void poweron(){
		System.out.println("开启电源");
	}
	
	protected void checkHardware(){
		System.out.println("硬件检查");
	}
	
	protected void loadOS(){
		System.out.println("载入操作系统");
	}
	
	protected void login(){
		System.out.println("登录");
	}
	
	/**
	 * 模板方法，定义算法的结构
	 */
	public final void startup(){
		System.out.println("------开机 start------");
		poweron();
		checkHardware();
		loadOS();
		login();
		System.out.println("------开机 end------");
	}
	
}
