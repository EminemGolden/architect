package com.akronrubin.architect.templatemethod.templatemethod2;

/**
 * 程序员电脑
 * @author Jason
 * QQ: 1476949583
 * @date 2016年1月6日
 * @version 1.0
 */
public class CoderComputer extends AbstractComputer {

	@Override
	protected void login() {
		System.out.println("程序员输入用户名和密码登录");
	}
	
}
