package com.akronrubin.architect.factorymethod.lib1client;


import com.akronrubin.architect.factorymethod.lib1.ExportFileApi;

public class ExportTextFile implements ExportFileApi {

	@Override
	public boolean export(String data) {
		System.out.println("数据："+data);
		System.out.println("导出了文本格式的数据...");
		return true;
	}

}
