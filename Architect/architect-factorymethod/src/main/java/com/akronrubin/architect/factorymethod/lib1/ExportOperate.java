package com.akronrubin.architect.factorymethod.lib1;

/**
 * 创建者 执行导出操作 创建产品
 * 
 * @author EminemGolden
 *
 */
public abstract class ExportOperate {

	/**
	 * 工厂方法，用于创建导出数据的文件对象
	 * 
	 * @return
	 */
	protected abstract ExportFileApi factoryMethod();

	public boolean export(String data) {
		// 导出特定文件
		ExportFileApi exportFileApi = factoryMethod();
		if (null == exportFileApi) {
			return false;
		}
		return exportFileApi.export(data);
	}

}
