package com.akronrubin.architect.factorymethod.lib1;

/**
 * 产品 
 * 导出数据文件（数据文件可能有，数据库文件、文本文件、XML文件、Excel文件等等）
 * 
 * @author EminemGolden
 *
 */
public interface ExportFileApi {

	public boolean export(String data);
	
}
