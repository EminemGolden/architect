package com.akronrubin.architect.factorymethod.lib1client;


import com.akronrubin.architect.factorymethod.lib1.ExportFileApi;
import com.akronrubin.architect.factorymethod.lib1.ExportOperate;

public class ExportTextFileOperate extends ExportOperate {

	@Override
	protected ExportFileApi factoryMethod() {
		return new ExportTextFile();
	}

}
