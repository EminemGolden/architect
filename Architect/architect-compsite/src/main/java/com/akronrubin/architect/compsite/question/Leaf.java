package com.akronrubin.architect.compsite.question;
/**
 * 叶子
 * @author wangdenghui
 *
 */
public class Leaf {

	private String name;

	public Leaf(String name) {
		this.name = name;
	}

	public void print(String preStr){
		System.out.println(preStr + "-"+name);
	}
}
