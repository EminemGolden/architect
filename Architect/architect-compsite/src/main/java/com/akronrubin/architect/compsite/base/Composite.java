package com.akronrubin.architect.compsite.base;

import java.util.ArrayList;
import java.util.List;

/**
 * 组合对象
 * @author wangdenghui
 *
 */
public class Composite extends Component {

	private List<Component> components = new ArrayList<Component>();

	@Override
	public void someOperation() {
		System.out.println("组件");
	}

	@Override
	public void addChild(Component child) {
		components.add(child);
	}

	@Override
	public void remove(Component component) {
		components.remove(component);
	}

	@Override
	public Component getComponet(int index) {
		return components.get(index);
	}

}
