package com.akronrubin.architect.compsite.solve;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Component {

	private String name;

	public Composite(String name) {
		super();
		this.name = name;
	}

	private List<Component> lists = new ArrayList<Component>();

	@Override
	public void addChild(Component child) {
		lists.add(child);
	}

	@Override
	public void remove(Component component) {
		lists.add(component);
	}

	@Override
	public Component getComponet(int index) {
		return lists.get(index);
	}

	@Override
	public void print(String preStr) {
		System.out.println(preStr + "+" + this.name);
		preStr += " ";
		for (Component component : lists) {
			component.print(preStr);
		}
	}

}
