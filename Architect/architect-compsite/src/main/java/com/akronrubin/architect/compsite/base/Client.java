package com.akronrubin.architect.compsite.base;
import org.junit.Test;
/**
 * 组合模式，不去分节点或者叶子类型(android View 和 ViewGroup对于互相添加就是用到这种处理模式)
 * 
 * @author wangdenghui
 *
 */
public class Client {

	@Test
	public void main() {

		Component root = new Composite();

		Component component1 = new Composite();
		Component component2 = new Composite();
		root.addChild(component1);
		root.addChild(component2);

		Leaf leaf1 = new Leaf();
		Leaf leaf2 = new Leaf();
		Leaf leaf3 = new Leaf();
		Leaf leaf4 = new Leaf();

		component1.addChild(leaf1);
		component1.addChild(leaf2);
		component2.addChild(leaf3);
		component2.addChild(leaf4);

	}
}
