package com.akronrubin.architect.compsite.base;

/**
 * 单个对象
 * 
 * @author wangdenghui
 *
 */
public class Leaf extends Component {

	@Override
	public void someOperation() {
		System.out.println("Leaf");
	}

}
