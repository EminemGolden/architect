package com.akronrubin.architect.compsite.solve;

public class Leaf extends Component {

	private String name;

	public Leaf(String name) {
		super();
		this.name = name;
	}

	@Override
	public void print(String preStr) {
		System.out.println(preStr + "-" + name);
	}

}
