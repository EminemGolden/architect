package com.akronrubin.architect.compsite.solve;

/**
 * 抽象组件类
 * 
 * @author wangdenghui
 *
 */
public abstract class Component {

	/**
	 * 添加子节点
	 * 
	 * @param child
	 */
	public void addChild(Component child) {
		// 用这种方式，如果子类实现了该方法，则会调用正确的方式，不然会抛出异常。限制被client错误调用
		throw new UnsupportedOperationException("对象不支持操作");
	}

	/**
	 * 删除子节点
	 * 
	 * @param component
	 */
	public void remove(Component component) {
		throw new UnsupportedOperationException("对象不支持操作");
	}

	/**
	 * 获取子节点
	 * 
	 * @param index
	 * @return
	 */
	public Component getComponet(int index) {
		throw new UnsupportedOperationException("对象不支持操作");
	}

	/**
	 * 打印
	 */
	abstract void print(String preStr);
}
