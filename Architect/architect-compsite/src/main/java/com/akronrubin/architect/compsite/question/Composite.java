package com.akronrubin.architect.compsite.question;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 组合
 * 
 * @author wangdenghui
 *
 */
public class Composite {

	private String name;

	private Collection<Composite> composites = new ArrayList<>();
	private Collection<Leaf> leafs = new ArrayList<>();

	public Composite(String name) {
		super();
		this.name = name;
	}

	/**
	 * 添加组合
	 * 
	 * @param composite
	 */
	public void addComposite(Composite composite) {
		composites.add(composite);
	}

	/**
	 * 添加叶子
	 * 
	 * @param leaf
	 */
	public void addLeaf(Leaf leaf) {
		leafs.add(leaf);
	}

	public void print(String preStr) {
		System.out.println(preStr + "+" + this.name);
		// 叶子对象
		preStr += " ";
		for (Leaf leaf : leafs) {
			leaf.print(preStr);
		}

		// 子组合对象
		for (Composite c : composites) {
			c.print(preStr);
		}
	}

}
