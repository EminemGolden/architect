package com.akronrubin.architect.compsite.question;
import org.junit.Test;
public class Client {

	@Test
	public void main() {

		Composite root = new Composite("服装");

		Composite c1 = new Composite("男装");
		Composite c2 = new Composite("女装");

		Leaf l1 = new Leaf("西装");
		Leaf l2 = new Leaf("夹克");
		Leaf l3 = new Leaf("短裙");
		Leaf l4 = new Leaf("套装");

		c1.addLeaf(l1);
		c1.addLeaf(l2);
		c2.addLeaf(l3);
		c2.addLeaf(l4);

		root.addComposite(c1);
		root.addComposite(c2);

		root.print("");
	}
}
