package com.akronrubin.architect.visitor.demo;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		Customer customer1 = new EnterpriseCustomer();
		customer1.setName("中国移动");

		Customer customer2 = new PersonalCustomer();
		customer2.setName("张家辉");

		ObjectStructure objectStructure = new ObjectStructure();
		objectStructure.addCustomer(customer1);
		objectStructure.addCustomer(customer2);

		objectStructure.handlerVisitor(new ServiceRequestVisitor());

	}

}
