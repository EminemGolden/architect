package com.akronrubin.architect.visitor.demo;

public interface  Visitor {

	 abstract void visitorEnterpriseCustomer(Customer customer);

	 abstract void visitorPersonalCustomer(Customer customer);
}
