package com.akronrubin.architect.visitor.base;

/**
 * 被访问者，提供访问者访问的方法
 * @author wangdenghui
 *
 */
public abstract class Element {
	/**
	 * 接受访问者访问自己的方法
	 * @param visitor 访问者实例
	 */
	public abstract void acceptVisitor(Visitor visitor);
	

}
