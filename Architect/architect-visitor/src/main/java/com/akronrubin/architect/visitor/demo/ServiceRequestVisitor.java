package com.akronrubin.architect.visitor.demo;


public class ServiceRequestVisitor implements Visitor{

	@Override
	public void visitorEnterpriseCustomer(Customer customer) {
		System.out.println(customer.getName()+" request enterprise service ");
	}

	@Override
	public void visitorPersonalCustomer(Customer customer) {
		System.out.println(customer.getName()+" request personal service ");
	}

}
