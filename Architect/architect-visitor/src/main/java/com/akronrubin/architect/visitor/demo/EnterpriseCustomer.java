package com.akronrubin.architect.visitor.demo;

public class EnterpriseCustomer extends Customer {
	private String linkMan;
	private String linkTelephone;
	private String registerAddress;

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getLinkTelephone() {
		return linkTelephone;
	}

	public void setLinkTelephone(String linkTelephone) {
		this.linkTelephone = linkTelephone;
	}

	public String getRegisterAddress() {
		return registerAddress;
	}

	public void setRegisterAddress(String registerAddress) {
		this.registerAddress = registerAddress;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visitorPersonalCustomer(this);
	}

}
