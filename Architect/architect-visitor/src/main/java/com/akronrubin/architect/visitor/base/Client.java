package com.akronrubin.architect.visitor.base;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		ConcreteElementA concreteElementA = new ConcreteElementA();
		ConcreteElementB concreteElementB = new ConcreteElementB();
		ObjectStructure structure = new ObjectStructure();
		structure.addElements(concreteElementA);
		structure.addElements(concreteElementB);
		Visitor visitor = new ConcreteVisitorA();
		structure.handleRequest(visitor);

	}

}
