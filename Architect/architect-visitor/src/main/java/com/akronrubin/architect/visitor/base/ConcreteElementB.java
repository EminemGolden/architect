package com.akronrubin.architect.visitor.base;

public class ConcreteElementB extends Element{

	@Override
	public void acceptVisitor(Visitor visitor) {
		System.out.println("被访问者"+ConcreteElementB.class.getSimpleName()+"接受访问者"+visitor.getClass().getSimpleName()+"的使用");
	}

}
