package com.akronrubin.architect.visitor.demo;

import java.util.ArrayList;
import java.util.List;

public class ObjectStructure {
	
	private List<Customer> customers = new ArrayList<Customer>();
	
	public void handlerVisitor(Visitor visitor){
		for (Customer customer : customers) {
			customer.accept(visitor);
		}
	}
	
	public void addCustomer(Customer customer){
		customers.add(customer);
	}

}
