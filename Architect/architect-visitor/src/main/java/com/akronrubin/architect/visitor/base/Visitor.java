package com.akronrubin.architect.visitor.base;
/**
 *  访问者  提供操作被访问者的方法
 * @author wangdenghui
 *
 */
public interface Visitor {

	void visitConcreteElementA(ConcreteElementA elementA);
	
	void visitConcreteElementB(ConcreteElementB elementB);
	
}
