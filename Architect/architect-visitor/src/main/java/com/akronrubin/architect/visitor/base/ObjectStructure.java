package com.akronrubin.architect.visitor.base;

import java.util.ArrayList;
import java.util.List;

public class ObjectStructure {

	private List<Element> elements = new ArrayList<Element>();

	public void handleRequest(Visitor visitor) {
		for (Element element : elements) {
			element.acceptVisitor(visitor);
		}
	}

	public void addElements(Element e) {
		elements.add(e);
	}

}
