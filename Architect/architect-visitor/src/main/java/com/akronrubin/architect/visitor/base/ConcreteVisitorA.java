package com.akronrubin.architect.visitor.base;

public class ConcreteVisitorA implements Visitor {

	@Override
	public void visitConcreteElementA(ConcreteElementA elementA) {
		System.out.println(ConcreteVisitorA.class.getSimpleName() + "访问ConcreteElementA");
	}

	@Override
	public void visitConcreteElementB(ConcreteElementB elementB) {
		System.out.println(ConcreteVisitorA.class.getSimpleName() + "访问ConcreteElementB");
	}

}
