package com.akronrubin.architect.proxy.case3;

/**
 * 订单接口的具体实现类
 * 
 * @author wangdenghui
 *
 */
public class OrderImpl implements IOrder {

	public OrderImpl(String productName, int orderNum, String orderUser) {
		super();
		this.productName = productName;
		this.orderNum = orderNum;
		this.orderUser = orderUser;
	}

	// 产品名称
	private String productName;
	// 订购的数量
	private int orderNum;
	// 创建订单的人员
	private String orderUser;

	@Override
	public String getProductName() {
		// TODO Auto-generated method stub
		return productName;
	}

	@Override
	public void setProductName(String productName, String user) {
		this.productName = productName;
	}

	@Override
	public int getOrderNum() {
		return orderNum;
	}

	@Override
	public void setOrderNum(int orderNum, String user) {
		this.orderNum = orderNum;
	}

	@Override
	public String getOrderUser() {
		return orderUser;
	}

	@Override
	public void setOrderUser(String orderUser, String user) {
		this.orderUser = orderUser;
	}

	@Override
	public String toString() {
		return "OrderImpl [productName=" + productName + ", orderNum=" + orderNum + ", orderUser=" + orderUser + "]";
	}

}
