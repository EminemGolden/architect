package com.akronrubin.architect.proxy.base;

public class Proxy implements ISubject {

	private RealSubjectImpl subject;

	public Proxy(RealSubjectImpl subject) {
		super();
		this.subject = subject;
	}

	@Override
	public void request() {
		// 在调用具体的目标对象的方法前，可以执行一些功能
		System.out.println("具体类核心业务被调用之前，代理先处理需要处理的业务.....");
		// 具体目标类的调用处
		subject.request();
		// 在调用具体的目标对象的方法后，可以执行一些功能
		System.out.println("具体类核心业务被调用之后，代理先处理需要处理的业务.....");
	}

}
