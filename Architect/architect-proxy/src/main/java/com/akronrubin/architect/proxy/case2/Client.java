package com.akronrubin.architect.proxy.case2;

import java.util.Collection;

public class Client {
	
	@Test
	public void main() {
		MockDataManager mockManager = new MockDataManager();
		Collection<Proxy> proxies = mockManager.getUserModels();
		for (Proxy proxy: proxies) {
			if(proxy.getDepId().equals("002")){
				System.out.println(proxy);
			}
			
		}
		
		
	}

}
