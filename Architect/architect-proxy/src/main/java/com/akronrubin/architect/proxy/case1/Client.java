package com.akronrubin.architect.proxy.case1;

import java.util.Collection;

public class Client {
	
	@Test
	public void main() {
		MockDataManager mockManager = new MockDataManager();
		Collection<UserModel> userModels = mockManager.getUserModels();
		for (UserModel userModel : userModels) {
			if(userModel.getDepId().equals("002")){
				System.out.println(userModel);
			}
			
		}
		
		
	}

}
