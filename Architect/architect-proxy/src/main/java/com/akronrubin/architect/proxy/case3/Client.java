package com.akronrubin.architect.proxy.case3;
/**
 *  静态代理实现方式
 * @author wangdenghui
 *
 */
public class Client {

	@Test
	public void main() {
		OrderImpl order = new OrderImpl("联想Y460笔记本电脑", 6, "QQ");
		OrderProxy orderProxy = new OrderProxy(order);
		System.out.println(orderProxy.getOrderUser());
		orderProxy.setOrderNum(100, "wechat");
		System.out.println(order);
	}

}
