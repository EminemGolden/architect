package com.akronrubin.architect.proxy.case1;

import java.util.ArrayList;
import java.util.Collection;

public class MockDataManager {
	public MockDataManager() {
		super();
		mockDepListData();
	}

	private Collection<UserModel> userModels = new ArrayList<>();
	private Collection<Department> departments = new ArrayList<>();

	public Collection<UserModel> getUserModels() {
		return userModels;
	}

	public Collection<Department> getDepartments() {
		return departments;
	}

	private void mockDepListData() {
		for (int i = 0; i < 4; i++) {
			Department department = new Department("00" + i, "部门" + i);
			departments.add(department);
			for (int j = 0; j < 4; j++) {
				UserModel userModel = new UserModel();
				userModel.setDepId(department.getDepId());
				userModel.setName(i + "部门员工" + j);
				if (j % 2 == 0) {
					userModel.setSex("男");
				} else {
					userModel.setSex("女");
				}
				userModel.setUserId(department.getDepId() + j);
				userModels.add(userModel);
			}
		}
	}

}
