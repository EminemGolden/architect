package com.akronrubin.architect.proxy.dynamicproxy;

import java.lang.reflect.Proxy;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		// 动态代理句柄
		DynamicProxyHandler handler = new DynamicProxyHandler();
		// 将代理对象根据具体的目标对象进行绑定
		OrderImpl order = new OrderImpl("iphone6s", 100, "王小强");

		handler.setTarget(order);
		// 代理对象
		// 动态生成了一个class，实现了OrderApi接口，使用这个class实例化了一个对象返回
		// 这就相当于有人在背后帮你自动写好了一个OrderProxy类，并使用这个类实例化了一个对象返回给你
		// 并且在OrderProxy类中的每一个方法中都动了手脚，在这些方法被调用时，都会回调InvocationHandler的invoke方法
		IOrder proxy = (IOrder) Proxy.newProxyInstance(order.getClass().getClassLoader(), // 目标对象的类加载器
				order.getClass().getInterfaces(), // 目标对象所属类的父接口
				handler); // 代理对象
		// System.out.println(orderApi);
		// 如：
		// OrderProxy autoClass = new OrderProxy(order, handler);
		// autoClass.getOrderNum();

		proxy.setOrderNum(160, "王强");
		System.out.println(proxy.getProductName());

		System.out.println(order);
	}

}
