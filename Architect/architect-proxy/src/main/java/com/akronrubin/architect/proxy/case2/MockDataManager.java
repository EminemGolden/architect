package com.akronrubin.architect.proxy.case2;

import java.util.ArrayList;
import java.util.Collection;

public class MockDataManager {
	public MockDataManager() {
		super();
		mockDepListData();
	}

	private Collection<Proxy> proxies = new ArrayList<>();
	private Collection<Department> departments = new ArrayList<>();

	public Collection<Proxy> getUserModels() {
		return proxies;
	}

	public Collection<Department> getDepartments() {
		return departments;
	}

	private void mockDepListData() {
		for (int i = 0; i < 4; i++) {
			Department department = new Department("00" + i, "部门" + i);
			departments.add(department);
			for (int j = 0; j < 4; j++) {
				UserModelImpl userModelImpl = new UserModelImpl();
				Proxy proxy = new Proxy(userModelImpl);
				proxy.setDepId(department.getDepId());
				proxy.setName(i + "部门员工" + j);
				if (j % 2 == 0) {
					proxy.setSex("男");
				} else {
					proxy.setSex("女");
				}
				proxy.setUserId(department.getDepId() + j);
				proxies.add(proxy);
			}
		}
	}

}
