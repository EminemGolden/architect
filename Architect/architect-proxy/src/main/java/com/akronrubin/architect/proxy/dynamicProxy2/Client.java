package com.akronrubin.architect.proxy.dynamicProxy2;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		//目标对象
		Order order = new Order("制服诱惑", 80, "李建彪");
		
		//代理句柄
		OrderInvocationHandler handler = new OrderInvocationHandler();
		handler.setTarget(order);
		
		//代理对象
		OrderApi proxy = new MyOrderProxy(order, handler);
		
		System.out.println(proxy.getOrderNum());
		
		proxy.setOrderNum(90, "刘文豪");
		
	}

}
