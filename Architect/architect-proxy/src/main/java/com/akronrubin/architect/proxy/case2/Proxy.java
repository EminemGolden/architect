package com.akronrubin.architect.proxy.case2;

/**
 * 虚代理，在代理类里面做被代理类的部分操作，这里由于获取UserModel对象不好模仿，故此代理了他全部的操作
 * 
 * @author EminemGolden
 *
 */
public class Proxy implements IUserModel {

	private UserModelImpl userModelImpl;

	public Proxy(UserModelImpl userModelImpl) {
		super();
		this.userModelImpl = userModelImpl;
	}

	@Override
	public String getUserId() {
		if (null == userModelImpl) {
			return "";
		}
		return userModelImpl.getUserId();
	}

	@Override
	public void setUserId(String userId) {
		if (null != userModelImpl) {
			userModelImpl.setUserId(userId);
		}
	}

	@Override
	public String getName() {
		if (null == userModelImpl) {
			return "";
		}
		return userModelImpl.getName();
	}

	@Override
	public void setName(String name) {
		if (null != userModelImpl) {
			userModelImpl.setName(name);
		}
	}

	@Override
	public String getDepId() {
		if (null == userModelImpl) {
			return "";
		}
		return userModelImpl.getDepId();
	}

	@Override
	public void setDepId(String depId) {
		if (null != userModelImpl) {
			userModelImpl.setDepId(depId);
		}
	}

	@Override
	public String getSex() {
		if (null == userModelImpl) {
			return "";
		}
		return userModelImpl.getSex();
	}

	@Override
	public void setSex(String sex) {
		if (null != userModelImpl) {
			userModelImpl.setSex(sex);
		}
	}
	
	@Override
	public String toString() {
		return "Proxy [userId=" + getUserId() + ", "
				+ "name=" + getName() + ", "
						+ "depId=" + getDepId() + ", "
				+ "sex=" + getSex() + "]";
	}


}
