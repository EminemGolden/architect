package com.akronrubin.architect.proxy.base;

/**
 * 目标接口，具体的目标对象和代理都会实现这个接口
 * 
 * @author EminemGolden
 *
 */
public interface ISubject {

	void request();

}
