package com.akronrubin.architect.proxy.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 动态代理句柄
 * 
 * @author wangdenghui
 *
 */
public class DynamicProxyHandler implements InvocationHandler {

	private IOrder order;

	/**
	 * 动态代理所代理的对象（目标对象）
	 * 
	 * @param order
	 */
	public void setTarget(IOrder order) {
		this.order = order;
	}

	public DynamicProxyHandler() {
		super();
	}

	/**
	 * method 目标对象要执行的方法 args 目标对象要执行的方法的参数列表
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (method.getName().startsWith("set")) {
			if (null != order && order.getOrderUser().equals(args[1])) {
				return method.invoke(order, args);
			} else {
				throw new IllegalArgumentException("没有权限进行相应操作！");
			}
		}
		return method.invoke(order, args);
	}

	public DynamicProxyHandler(IOrder order) {
		super();
		this.order = order;
	}

}
