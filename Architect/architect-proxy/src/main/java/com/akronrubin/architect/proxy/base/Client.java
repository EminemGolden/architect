package com.akronrubin.architect.proxy.base;

public class Client {

	@Test
	public void main() {
		RealSubjectImpl subject = new RealSubjectImpl();
		ISubject proxy = new Proxy(subject);
		proxy.request();
		
	}

}
