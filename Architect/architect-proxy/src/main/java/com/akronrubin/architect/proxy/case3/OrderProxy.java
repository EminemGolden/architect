package com.akronrubin.architect.proxy.case3;

/**
 * 订单代理类
 * 
 * @author wangdenghui
 *
 */
public class OrderProxy implements IOrder {

	private IOrder order;

	private boolean operationLegal(String user) {
		return (null != order && order.getOrderUser().equals(user));
	}

	public OrderProxy(IOrder order) {
		super();
		this.order = order;
	}

	@Override
	public String getProductName() {
		return order.getProductName();
	}

	@Override
	public void setProductName(String productName, String user) {
		if (operationLegal(user)) {
			this.setProductName(productName, user);
		} else {
			throw new IllegalAccessError("没有权限进行该操作！");
		}
	}

	@Override
	public int getOrderNum() {
		return order.getOrderNum();
	}

	@Override
	public void setOrderNum(int orderNum, String user) {
		if (operationLegal(user)) {
			this.setOrderNum(orderNum, user);
		} else {
			throw new IllegalAccessError("没有权限进行该操作！");
		}
	}

	@Override
	public String getOrderUser() {
		return order.getOrderUser();
	}

	@Override
	public void setOrderUser(String orderUser, String user) {
		if (operationLegal(user)) {
			this.setOrderUser(orderUser, user);
		} else {
			throw new IllegalAccessError("没有权限进行该操作！");
		}
	}

}
