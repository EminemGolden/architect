package com.akronrubin.architect.proxy.base;

public class RealSubjectImpl implements ISubject{

	@Override
	public void request() {
		System.out.println("======具体实现类被调用========");
	}

}
