package com.akronrubin.architect.mediator.case1;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		//中介者，主板
		MainBoard mediator = new MainBoard();
		
		//同事类
		CDDriver cd = new CDDriver(mediator);
		CPU cpu = new CPU(mediator);
		VideoCard vc = new VideoCard(mediator);
		SoundCard sc = new SoundCard(mediator);
		
		mediator.setCdDriver(cd);
		mediator.setCpu(cpu);
		mediator.setVideoCard(vc);
		mediator.setSoundCard(sc);
		
		//光驱读数据
		cd.readCD();
		
	}

}
