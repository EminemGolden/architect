package com.akronrubin.architect.mediator.base;
/**
 * 同事具体实现类
 * @author EminemGolden
 *
 */
public class ConcreateColleagueA extends Colleague{

	public ConcreateColleagueA(IMediator mediator) {
		super(mediator);
	}
	
	public void someOperation(){
		getMediator().changed(this);
	}

}
