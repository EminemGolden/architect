package com.akronrubin.architect.mediator.base;

/**
 * 具体同事实现类B
 * 
 * @author EminemGolden
 *
 */
public class ConcreateColleagueB extends Colleague {

	public ConcreateColleagueB(IMediator mediator) {
		super(mediator);
	}

	public void someOperation() {
		getMediator().changed(this);
	}

}
