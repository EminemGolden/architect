package com.akronrubin.architect.mediator.base;

public class ConcreateMediator implements IMediator {

	private ConcreateColleagueA concreateMdeiatorA;
	private ConcreateColleagueB concreateMdeiatorB;

	public ConcreateColleagueA getConcreateMdeiatorA() {
		return concreateMdeiatorA;
	}

	public void setConcreateMdeiatorA(ConcreateColleagueA concreateMdeiatorA) {
		this.concreateMdeiatorA = concreateMdeiatorA;
	}

	public ConcreateColleagueB getConcreateMdeiatorB() {
		return concreateMdeiatorB;
	}

	public void setConcreateMdeiatorB(ConcreateColleagueB concreateMdeiatorB) {
		this.concreateMdeiatorB = concreateMdeiatorB;
	}

	@Override
	public void changed(Colleague colleague) {
		System.out.println("A------------------");
		System.out.println("B------------------");
	}

}
