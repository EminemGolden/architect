package com.akronrubin.architect.mediator.base;
/**
 * 同事接口
 * @author EminemGolden
 *
 */
public abstract class Colleague {

	private IMediator mediator;

	public Colleague(IMediator mediator) {
		this.mediator = mediator;
	}

	public IMediator getMediator() {
		return mediator;
	}

}
