package com.akronrubin.architect.mediator.base;

/**
 * 中介者
 * 
 * @author EminemGolden
 *
 */
public interface IMediator {

	public void changed(Colleague colleague);

}
