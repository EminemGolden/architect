package com.akronrubin.architect.mediator.case2;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		//DepUserMediator mediator = DepUserMediator.getInstance();
		
		//这个员工离职
		User user = new User();
		user.setUserId("U01");
		user.dismiss();
		
		//mediator.showUserDeps();
		
		//撤销部门
		Department dep = new Department();
		dep.setDepId("DEP02");
		dep.deleteDep();
		
		System.out.println("--------------");
		//mediator.showUserDeps();
		
	}

}
