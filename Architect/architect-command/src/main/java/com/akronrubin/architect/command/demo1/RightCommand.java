package com.akronrubin.architect.command.demo1;

public class RightCommand implements ICommand {

	private TetrisMachine tetrisMachine;

	public RightCommand(TetrisMachine tetrisMachine) {
		super();
		this.tetrisMachine = tetrisMachine;
	}

	@Override
	public void excute() {
		tetrisMachine.toRight();
	}

}
