package com.akronrubin.architect.command.base;

/**
 * 命令具体实现者
 * 
 * @author wangdenghui
 *
 */
public class CommandImpl implements ICommand {
	private Receiver receiver;

	public CommandImpl(Receiver receiver) {
		super();
		this.receiver = receiver;
	}

	@Override
	public void action() {
		receiver.invock();
	}

}
