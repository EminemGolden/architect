package com.akronrubin.architect.command.demo1;

public class TransformCommand implements ICommand {

	private TetrisMachine tetrisMachine;

	public TransformCommand(TetrisMachine tetrisMachine) {
		super();
		this.tetrisMachine = tetrisMachine;
	}

	@Override
	public void excute() {
		tetrisMachine.transform();
	}

}
