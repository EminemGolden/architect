package com.akronrubin.architect.command.demo1;

public class LeftCommand implements ICommand {

	private TetrisMachine tetrisMachine;

	public LeftCommand(TetrisMachine tetrisMachine) {
		super();
		this.tetrisMachine = tetrisMachine;
	}

	@Override
	public void excute() {
		tetrisMachine.toLeft();
	}

}
