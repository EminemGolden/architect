package com.akronrubin.architect.command.demo1;
import org.junit.Test;
/**
 * 加强理解的demo button被点击向左、向右、向下、反转按钮，执行对应命令，调用机器实现具体操作
 * 
 * @author wangdenghui
 *
 */
public class Client {

	@Test
	public void main() {
		TetrisMachine tetrisMachine = new TetrisMachine();
		LeftCommand leftCommand = new LeftCommand(tetrisMachine);
		RightCommand rightCommand = new RightCommand(tetrisMachine);
		FallCommand fallCommand = new FallCommand(tetrisMachine);
		TransformCommand transformCommand = new TransformCommand(tetrisMachine);
		
		Button button = new Button();
		button.setFallCommand(fallCommand);
		button.setLeftCommand(leftCommand);
		button.setRightCommand(rightCommand);
		button.setTransformCommand(transformCommand);

		button.toFallDown();
		button.toLeft();
		button.toRight();
		button.transform();

	}

}
