package com.akronrubin.architect.command.base;

/**
 * 命令 针对不同的对象实现不一样的命令，负责通知具体实现对象执行对应的业务逻辑操作
 * 
 * @author wangdenghui
 *
 */
public interface ICommand {

	public void action();

}
