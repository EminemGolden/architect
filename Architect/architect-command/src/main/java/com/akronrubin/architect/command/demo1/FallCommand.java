package com.akronrubin.architect.command.demo1;

public class FallCommand implements ICommand {

	private TetrisMachine tetrisMachine;

	public FallCommand(TetrisMachine tetrisMachine) {
		super();
		this.tetrisMachine = tetrisMachine;
	}

	@Override
	public void excute() {
		tetrisMachine.fastToBottom();
	}

}
