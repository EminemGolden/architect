package com.akronrubin.architect.command.base;

/**
 * 接受者  接受命令的对象，具体业务的实现类
 * 
 * @author wangdenghui
 *
 */
public class Receiver {

	/**
	 * 行动方法，执行具体的逻辑
	 */
	public void invock() {
		System.out.println("具体接受者，执行具体业务逻辑");
	}

}
