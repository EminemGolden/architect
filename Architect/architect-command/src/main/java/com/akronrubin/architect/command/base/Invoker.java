package com.akronrubin.architect.command.base;

/**
 * 请求者 请求执行对应的业务逻辑，将命令对象发给接受者，达到请求的需求操作
 * 
 * @author wangdenghui
 *
 */
public class Invoker {

	private ICommand command;

	public Invoker(ICommand command) {
		super();
		this.command = command;
	}

	public void action() {
		command.action();
	}
}
