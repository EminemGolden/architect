package com.akronrubin.architect.command.demo1;

public interface ICommand {

	void excute();

}
