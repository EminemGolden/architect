package com.akronrubin.architect.command.demo1;

public class Button {

	private LeftCommand leftCommand ;
	private RightCommand rightCommand;
	private FallCommand fallCommand;
	private TransformCommand transformCommand;

	public void toRight() {
		rightCommand.excute();
	}

	public void toLeft() {
		leftCommand.excute();
	}

	public void toFallDown() {
		fallCommand.excute();
	}

	public void transform() {
		transformCommand.excute();
	}

	public void setLeftCommand(LeftCommand leftCommand) {
		this.leftCommand = leftCommand;
	}

	public void setRightCommand(RightCommand rightCommand) {
		this.rightCommand = rightCommand;
	}

	public void setFallCommand(FallCommand fallCommand) {
		this.fallCommand = fallCommand;
	}

	public void setTransformCommand(TransformCommand transformCommand) {
		this.transformCommand = transformCommand;
	}

}
