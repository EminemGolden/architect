package com.akronrubin.architect.command.base;
import org.junit.Test;
/**
 * 命令模式
 * 
 * @author wangdenghui
 *
 */
public class Client {

	@Test
	public void main() {
		
		Receiver receiver = new Receiver();
		ICommand command = new CommandImpl(receiver);
		Invoker invoker = new Invoker(command);
		invoker.action();
	}

}
