package com.akronrubin.architect.command.demo1;

public class TetrisMachine {
	
	public void toLeft(){
		System.out.println("向左");
	}
	
	public void toRight(){
		System.out.println("向右");
	}
	
	public void fastToBottom(){
		System.out.println("快下");
	}
	
	public void transform(){
		System.out.println("变形");
	}

}
