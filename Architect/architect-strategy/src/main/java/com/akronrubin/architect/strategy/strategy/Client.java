package com.akronrubin.architect.strategy.strategy;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		Strategy s1 = new ConcreteStrategyA();
		Strategy s2 = new ConcreteStrategyB();
		Context context = new Context(s2);
		context.contextInterface();
	}

}
