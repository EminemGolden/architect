package com.akronrubin.architect.strategy.strategy2;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		CalculateStrategy s1 = new BusStrategy();
		CalculateStrategy s2 = new SubwayStrategy();
		CalculateStrategy s3 = new TaxiStrategy();
		TrafficCalculator c = new TrafficCalculator();
		c.setStrategy(s3);
		int r = c.calculate(20);
		System.out.println("交通费用："+r);
	}

}
