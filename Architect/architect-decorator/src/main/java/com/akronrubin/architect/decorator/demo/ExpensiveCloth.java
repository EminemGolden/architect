package com.akronrubin.architect.decorator.demo;

public class ExpensiveCloth extends Cloth {

	public ExpensiveCloth(Person person) {
		super(person);
	}

	@Override
	public void dress() {
		person.dress();
		dressShirt();
		dressJean();
	}
	
	public void dressShirt() {
		System.out.println("添加外套");
	}

	public void dressJean() {
		System.out.println("穿上外裤");
	}

}
