package com.akronrubin.architect.decorator.demo3;

import java.util.Date;

/**
 * 奖金最基础的具体实现类
 * 
 * @author wangdenghui
 *
 */
public class Prize extends Component {

	@Override
	protected double calcPrize(String user, Date begin, Date end) {
		return 0;
	}

}
