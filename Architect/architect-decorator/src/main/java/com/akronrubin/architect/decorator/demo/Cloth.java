package com.akronrubin.architect.decorator.demo;

public abstract class Cloth extends Person{

	protected Person person;

	public Cloth(Person person) {
		super();
		this.person = person;
	}
	
	@Override
	public void dress() {
		super.dress();
	}
}
