package com.akronrubin.architect.decorator.base;

/**
 * 装饰者的具体实现类
 * 
 * @author wangdenghui
 *
 */
public class ConcreateDecoratorB extends Decorator {

	public ConcreateDecoratorB(Conponent conponent) {
		super(conponent);
	}

	@Override
	protected void operate() {
		operateA();
		super.operate();
		operateB();
	}

	/**
	 * 装饰者添加的具体方法
	 */
	public void operateA() {
		System.out.println("装饰者的具体实现" + ConcreateDecoratorB.class.getSimpleName() + " 的附加业务方法operateA");
	}

	/**
	 * 装饰者添加的具体方法
	 */
	public void operateB() {
		System.out.println("装饰者的具体实现" + ConcreateDecoratorB.class.getSimpleName() + " 的附加业务方法operateB");
	}

}
