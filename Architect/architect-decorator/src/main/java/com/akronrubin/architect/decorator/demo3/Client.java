package com.akronrubin.architect.decorator.demo3;
import org.junit.Test;
/**
 * 装饰者模式精髓：实现对象累加功能
 * 
 * @author wangdenghui
 *
 */

public class Client {

	@Test
	public void main() {
		// 装饰对象
		// 基层，月奖金、累计奖金
		PrizeDecorator monthPrizeDecorator = new MonthPrizeDecorator(new Prize());
		// d2装饰d1，功能组合
		PrizeDecorator sumPrizeDecorator = new SumPrizeDecorator(monthPrizeDecorator);
		double r1 = sumPrizeDecorator.calcPrize("TFBoys", null, null);
		System.out.println("---------TFBoys应得奖金：" + r1 + "--------------------------------");

		double r2 = sumPrizeDecorator.calcPrize("六小龄童", null, null);
		System.out.println("---------六小龄童：" + r2 + "--------------------------------");

		// 管理人员
		// 团队奖金，功能组合
		PrizeDecorator groupPrizeDecorator = new GroupPrizeDecorator(sumPrizeDecorator);
		double r3 = groupPrizeDecorator.calcPrize("赵薇", null, null);
		System.out.println("---------赵薇：" + r3 + "--------------------------------");

	}

}
