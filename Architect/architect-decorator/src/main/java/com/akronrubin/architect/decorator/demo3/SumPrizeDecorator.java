package com.akronrubin.architect.decorator.demo3;

import java.util.Date;

/**
 * 累计奖金实现类 每个人累计奖金 = 总的回款额 * 0.1%
 * 
 * @author wangdenghui
 *
 */
public class SumPrizeDecorator extends PrizeDecorator {

	public SumPrizeDecorator(Component component) {
		super(component);
	}

	@Override
	protected double calcPrize(String user, Date begin, Date end) {
		return sumPrize(user, begin, end) + super.calcPrize(user, begin, end);
	}

	/**
	 * 累计奖金 = 总的回款额 * 0.1%
	 * 
	 * @param user
	 * @param begin
	 * @param end
	 * @return
	 */
	private double sumPrize(String user, Date begin, Date end) {
		double prize = 100000 * 0.001;
		System.out.println(user + "累计奖金：" + prize);
		return prize;
	}

}
