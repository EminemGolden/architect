package com.akronrubin.architect.decorator.demo3;

import java.util.Date;

import com.egolden.architect.decorator.demo2.TempDB;

/**
 * 团队奖金 团队奖金 = 团队总销售额 * 1%
 * 
 * @author wangdenghui
 *
 */
public class GroupPrizeDecorator extends PrizeDecorator {

	public GroupPrizeDecorator(Component component) {
		super(component);
	}

	@Override
	protected double calcPrize(String user, Date begin, Date end) {
		// TODO Auto-generated method stub
		return groupPrize(user, begin, end) + super.calcPrize(user, begin, end);
	}

	/**
	 * 团队奖金 = 团队总销售额 * 1%
	 * 
	 * @param user
	 * @param begin
	 * @param end
	 * @return
	 */
	private double groupPrize(String user, Date begin, Date end) {
		double prize = 0;
		for (double d : TempDB.saleMoney.values()) {
			prize += d;
		}
		prize = prize * 0.01;
		System.out.println(user + "团队奖金：" + prize);
		return prize;
	}

}
