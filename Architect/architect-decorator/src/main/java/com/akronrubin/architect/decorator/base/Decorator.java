package com.akronrubin.architect.decorator.base;

/**
 * 装饰者抽象类，继承需要被装饰的抽象类，拥有对应被装饰者的基础功能
 * 
 * @author wangdenghui
 *
 */
public abstract class Decorator extends Conponent {

	/**
	 * 被装饰者对象的引用
	 */
	protected Conponent conponent;

	public Decorator(Conponent conponent) {
		this.conponent = conponent;
	}

	@Override
	protected void operate() {
		//调用被装饰者的方法
		conponent.operate();
	}

}
