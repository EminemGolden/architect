package com.akronrubin.architect.decorator.demo2;

import java.util.HashMap;
import java.util.Map;

/**
 * 临时数据库
 * 
 * @author wangdenghui
 *
 */
public class TempDB {

	public static Map<String, Double> saleMoney = new HashMap<String, Double>();

	static {
		saleMoney.put("TFBoys", 10000.0);
		saleMoney.put("六小龄童", 30000.0);
		saleMoney.put("赵薇", 50000.0);
	}

}
