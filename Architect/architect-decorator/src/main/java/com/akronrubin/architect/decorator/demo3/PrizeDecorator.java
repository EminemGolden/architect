package com.akronrubin.architect.decorator.demo3;

import java.util.Date;

/**
 * 奖金计算具体装饰者
 * 
 * @author wangdenghui
 *
 */
public abstract class PrizeDecorator extends Component {

	/**
	 * 必须持有被修饰对象的引用
	 */
	public Component component;

	public PrizeDecorator(Component component) {
		super();
		this.component = component;
	}

	@Override
	protected double calcPrize(String user, Date begin, Date end) {
		return component.calcPrize(user, begin, end);
	}

}
