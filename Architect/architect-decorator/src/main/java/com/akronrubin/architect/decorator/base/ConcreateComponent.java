package com.akronrubin.architect.decorator.base;

/**
 * 被装饰者的具体实现
 * 
 * @author wangdenghui
 *
 */
public class ConcreateComponent extends Conponent {

	@Override
	protected void operate() {
		// 实现具体逻辑
		System.out.println("被装饰者" + ConcreateComponent.class.getSimpleName() + " 具体逻辑");
	}

}
