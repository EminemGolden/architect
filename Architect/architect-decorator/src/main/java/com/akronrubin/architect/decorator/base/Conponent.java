package com.akronrubin.architect.decorator.base;

/**
 * 被装饰对象的抽象类
 * 
 * @author wangdenghui
 *
 */
public abstract class Conponent {
	
	protected abstract void operate();

}
