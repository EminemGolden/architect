package com.akronrubin.architect.decorator.demo3;

import java.util.Date;

import com.egolden.architect.decorator.demo2.TempDB;

/**
 * 每个人当月业务奖金 = 当月销售额 * 3%
 * 
 * @author wangdenghui
 *
 */
public class MonthPrizeDecorator extends PrizeDecorator {

	public MonthPrizeDecorator(Component component) {
		super(component);
	}

	@Override
	protected double calcPrize(String user, Date begin, Date end) {
		return monthPrize(user, begin, end) + super.calcPrize(user, begin, end);
	}

	/**
	 * 当月业务奖金 = 当月销售额 * 3%
	 * 
	 * @param user
	 * @param begin
	 * @param end
	 * @return
	 */
	private double monthPrize(String user, Date begin, Date end) {
		double prize = TempDB.saleMoney.get(user) * 0.03;
		System.out.println(user + "当月业务奖金：" + prize);
		return prize;
	}

}
