package com.akronrubin.architect.decorator.demo2;

import java.util.Date;

/**
 * 计算奖金
 * 
 * @author wangdenghui
 *
 */
public class Prize {

	/**
	 * 计算默认在某段时间内的奖金
	 * 
	 * @param user
	 * @param begin
	 * @param end
	 * @return
	 */
	public double calcPrize(String user, Date begin, Date end) {
		double prize = 0;
		// 1.每个人当月业务奖金 = 当月销售额 * 3%
		prize = monthPrize(user, begin, end);
		// 2.每个人累计奖金 = 总的回款额 * 0.1%
		prize += sumPrize(user, begin, end);
		// 3.团队奖金 = 团队总销售额 * 1%
		if (isManager(user)) {
			prize += groupPrize(user, begin, end);
		}

		return prize;
	}

	/**
	 * 是否是经理
	 * 
	 * @param user
	 * @return
	 */
	private boolean isManager(String user) {
		if ("赵薇".equals(user)) {
			return true;
		}
		return false;
	}

	/**
	 * 团队奖金 = 团队总销售额 * 1%
	 * 
	 * @param user
	 * @param begin
	 * @param end
	 * @return
	 */
	private double groupPrize(String user, Date begin, Date end) {
		double prize = 0;
		for (double d : TempDB.saleMoney.values()) {
			prize += d;
		}
		prize = prize * 0.01;
		System.out.println(user + "团队奖金：" + prize);
		return prize;
	}

	/**
	 * 累计奖金 = 总的回款额 * 0.1%
	 * 
	 * @param user
	 * @param begin
	 * @param end
	 * @return
	 */
	private double sumPrize(String user, Date begin, Date end) {
		double prize = 100000 * 0.001;
		System.out.println(user + "累计奖金：" + prize);
		return prize;
	}

	/**
	 * 当月业务奖金 = 当月销售额 * 3%
	 * 
	 * @param user
	 * @param begin
	 * @param end
	 * @return
	 */
	private double monthPrize(String user, Date begin, Date end) {
		double prize = TempDB.saleMoney.get(user) * 0.03;
		System.out.println(user + "当月业务奖金：" + prize);
		return prize;
	}

}
