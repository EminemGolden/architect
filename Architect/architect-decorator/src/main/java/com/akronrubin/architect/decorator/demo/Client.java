package com.akronrubin.architect.decorator.demo;
import org.junit.Test;
/**
 * 案例：给男孩包装，添加衣服 
 * 
 * @author wangdenghui
 *
 */
public class Client {

	@Test
	public void main() {
		ExpensiveCloth cloth = new ExpensiveCloth(new Boy());
		cloth.dress();
	}
}
