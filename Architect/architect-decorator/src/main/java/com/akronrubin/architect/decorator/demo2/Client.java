package com.akronrubin.architect.decorator.demo2;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		Prize p = new Prize();
		
		double d1 = p.calcPrize("TFBoys", null, null);
		System.out.println("---------TFBoys应得奖金："+d1);
		
		double d2 = p.calcPrize("六小龄童", null, null);
		System.out.println("---------六小龄童应得奖金："+d2);
		
		double d3 = p.calcPrize("赵薇", null, null);
		System.out.println("---------赵薇应得奖金："+d3);
	}

}
