package com.akronrubin.architect.decorator.demo3;

import java.util.Date;

/**
 * 奖金抽象接口
 * 
 * @author wangdenghui
 *
 */
public abstract class Component {

	protected abstract double calcPrize(String user, Date begin, Date end);

}
