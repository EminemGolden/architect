package com.akronrubin.architect.decorator.base;
import org.junit.Test;
/**
 * 装饰模式：和静态代理模式比较类似，但是，代理模式注重实现业务逻辑的代理操作；装饰者注重功能拓展
 * 
 * @author wangdenghui
 *
 */
public class Client {

	@Test
	public void main() {
		// 被装饰中者
		Conponent conponent = new ConcreateComponent();
		// 装饰者，添加业务操作
		ConcreateDecoratorA decorator = new ConcreateDecoratorA(conponent);
		decorator.operate();
		System.out.println("------------------------------------------");
		ConcreateDecoratorB decoratorB = new ConcreateDecoratorB(conponent);
		decoratorB.operate();
	}

}
