package com.akronrubin.architect.observable.observer5;

/**
 * 观察者
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月26日
 * @version 1.0
 */
public interface Observer {
	
	/**
	 * 设置职务
	 * @param job
	 */
	void setJob(String job);
	
	String getJob();
	
	/**
	 * 更新方法（被通知方法）
	 * @param subject 空气对象（目标对象）
	 */
	void update(Suject subject);
	
	
}
