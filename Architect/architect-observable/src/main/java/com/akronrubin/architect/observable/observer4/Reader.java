package com.akronrubin.architect.observable.observer4;

import java.util.Observable;

/**
 * 读者（具体的订阅者）
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月26日
 * @version 1.0
 */
public  class Reader implements java.util.Observer {

	// 读者姓名
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void update(Observable o, Object arg) {
		//System.out.println(name+"收到报纸了，打开看看，内容是："+((NewsPaper)o).getContent());
		System.out.println(name+"收到报纸了，打开看看，内容是："+arg);//如果是拉模式，arg为null
	}

}
