package com.akronrubin.architect.observable.observer5;

/**
 * 具体的观察者
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月26日
 * @version 1.0
 */
public class WatcherObserver implements Observer{

	//职务
	private String job;
	
	@Override
	public void setJob(String job) {
		this.job = job;
	}

	@Override
	public String getJob() {
		return job;
	}

	@Override
	public void update(Suject subject) {
		System.out.println(job+"收到通知，当前污染级别为："+subject.getPolluteLevel());
		if(job.equals("监测人员")){
			System.out.println("老实做记录...");
		}else if(job.equals("预警人员")){
			System.out.println("做预警...");
		}else if(job.equals("监测部门领导")){
			System.out.println("包小三...");
		}
	}

}
