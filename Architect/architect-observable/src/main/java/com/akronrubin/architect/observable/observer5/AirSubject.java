package com.akronrubin.architect.observable.observer5;

/**
 * 具体的空气（具体的目标）
 * 
 * @author Jason QQ: 1476949583
 * @date 2015年12月26日
 * @version 1.0
 */
public class AirSubject extends Suject {

	/**
	 * 污染级别
	 */
	private int polluteLevel;

	@Override
	public int getPolluteLevel() {
		return polluteLevel;
	}

	public void setPolluteLevel(int polluteLevel) {
		this.polluteLevel = polluteLevel;
		// 当污染级别发生改变时，观察者
		notifyObservers();
	}

	@Override
	public void notifyObservers() {
		for (Observer observer : observers) {
			// 0 1 2
			//雾霾监测
			//>=0 正常，监测人员做记录 
			//>=1 轻度污染，监测人员做记录，通知预警人员 
			//>=2 高度污染，监测人员做记录，通知预警人员，通知监测领导 
			if (this.polluteLevel >= 0) {
				if("监测人员".equals(observer.getJob())){
					observer.update(this);
				}
			}

			if (this.polluteLevel >= 1) {
				if("预警人员".equals(observer.getJob())){
					observer.update(this);
				}
			}

			if (this.polluteLevel >= 2) {
				if("监测部门领导".equals(observer.getJob())){
					observer.update(this);
				}
			}
		}
	}

}
