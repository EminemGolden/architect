package com.akronrubin.architect.observable.observer4;

/**
 * 报纸，具体的目标
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月26日
 * @version 1.0
 */
public class NewsPaper extends java.util.Observable{

	/**
	 * 报纸内容
	 */
	private String content;
	
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
		//使用Java对观察者模式的支持，这句话不能少
		setChanged();
		//通知所有的读者
		//notifyObservers();
		//推模式
		notifyObservers(content);
	}
	
	
	
}
