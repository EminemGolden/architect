package com.akronrubin.architect.observable.observer4;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		//目标对象
		NewsPaper subject = new NewsPaper();
		
		//观察者
		Reader reader1 = new Reader();
		reader1.setName("李俊扬");
		
		Reader reader2 = new Reader();
		reader2.setName("阿城");
		
		//注册
		subject.addObserver(reader1);
		subject.addObserver(reader2);
		
		subject.setContent("环球时报，阿城是李嘉诚私生子？");
		
		System.out.println("------------");
		subject.deleteObserver(reader2);
		
		subject.setContent("环球时报，阿城确实是李嘉诚失散多年的私生子!");
	}

}
