package com.akronrubin.architect.observable.observer2;

/**
 * 报纸，具体的目标
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月26日
 * @version 1.0
 */
public class NewsPaper extends Subject{

	/**
	 * 报纸内容
	 */
	private String content;
	
	@Override
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
		//通知所有的读者
		notifyObservers();
	}
	
	
	
}
