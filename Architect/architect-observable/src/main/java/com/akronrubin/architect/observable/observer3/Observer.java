package com.akronrubin.architect.observable.observer3;

/**
 * 观察者接口
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月26日
 * @version 1.0
 */
public interface Observer {

	/**
	 * 更新（当目标对象的状态发生改变时，这个方法会被调用）
	 * @param content 发生改变的属性
	 */
	void update(String content);
	
}
