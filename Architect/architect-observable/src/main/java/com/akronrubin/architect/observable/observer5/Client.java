package com.akronrubin.architect.observable.observer5;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		//目标
		AirSubject subject = new AirSubject();
		
		//观察者
		WatcherObserver observer1 = new WatcherObserver();
		observer1.setJob("监测人员");
		
		WatcherObserver observer2 = new WatcherObserver();
		observer2.setJob("预警人员");
		
		WatcherObserver observer3 = new WatcherObserver();
		observer3.setJob("监测部门领导");
		
		//订阅
		subject.attach(observer1);
		subject.attach(observer2);
		subject.attach(observer3);
		
		System.out.println("当空气质量正常时：-------------");
		subject.setPolluteLevel(0);
		
		System.out.println("当空气质量轻度污染时：-------------");
		subject.setPolluteLevel(1);
		
		System.out.println("当空气质量重度污染时：-------------");
		subject.setPolluteLevel(2);
	}
	
	

}
