package com.akronrubin.architect.observable.observer3;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		//具体的目标对象
		NewsPaper subject = new NewsPaper();
		//观察者
		Reader reader1 = new Reader();
		reader1.setName("李俊杨");
		subject.attach(reader1);
		
		Reader reader2 = new Reader();
		reader2.setName("宋卫星");
		subject.attach(reader2);
		
		//出新报纸
		subject.setContent("最新一期环球时报，老宁荣登福布斯排行榜首富...");
		
		//不要我了
		subject.detach(reader2);
		
		subject.setContent("最新一期环球时报，老宁登上了男人装...");
	}

}
