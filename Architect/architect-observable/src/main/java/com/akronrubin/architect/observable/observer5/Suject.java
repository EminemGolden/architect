package com.akronrubin.architect.observable.observer5;

import java.util.ArrayList;
import java.util.List;

/**
 * 空气目标
 * @author Jason
 * QQ: 1476949583
 * @date 2015年12月26日
 * @version 1.0
 */
public abstract class Suject {

	//观察者列表
	protected List<Observer> observers = new ArrayList<Observer>();
	
	/**
	 * 注册
	 * @param observer
	 */
	public void attach(Observer observer){
		observers.add(observer);
	}
	
	/**
	 * 取消注册
	 * @param observer
	 */
	public void dettach(Observer observer){
		observers.remove(observer);
	}
	
	/**
	 * 通知观察者
	 */
	public abstract void notifyObservers(); 
	
	/**
	 * 获取污染级别
	 * @return
	 */
	public abstract int getPolluteLevel();
	
}
