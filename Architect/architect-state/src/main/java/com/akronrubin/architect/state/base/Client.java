package com.akronrubin.architect.state.base;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		Context context = new Context();
		IState state = new ConcreteStateB();
		context.setState(state);
		context.request("处理消息");
	}
}
