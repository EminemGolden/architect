package com.akronrubin.architect.state.base;

/**
 * 具体的状态A
 * 
 * @author wangdenghui
 *
 */
public class ConcreteStateA implements IState {

	@Override
	public void handler(String tag) {
		System.out.println("A" + tag);
	}

}
