package com.akronrubin.architect.state.resolvequestion;
/**
 * 状态接口
 * @author wangdenghui
 *
 */
public interface ITVState {

	/**
	 * 下一频道
	 */
	public void nextChannel();

	/**
	 * 上一频道
	 */
	public void prevChannel();

	/**
	 * 调高音量
	 */
	public void turnUp();

	/**
	 * 调低音量
	 */
	public void turnDown();
}
