package com.akronrubin.architect.state.base;

/**
 * 上下文，负责与状态对象进行交互
 * 
 * @author wangdenghui
 *
 */
public class Context {

	private IState state;

	public void setState(IState state) {
		this.state = state;
	}

	public void request(String str) {
		state.handler(str);
	}

}
