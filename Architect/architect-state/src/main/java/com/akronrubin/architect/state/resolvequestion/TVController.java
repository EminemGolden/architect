package com.akronrubin.architect.state.resolvequestion;
/**
 * 状态控制类
 * @author wangdenghui
 *
 */
public class TVController {

	private ITVState state = new PowerOffState();

	public void powerOn() {
		state = new PowerOnState();
		System.out.println("开机");
	}

	public void powerOff() {
		state = new PowerOffState();
		System.out.println("关机");
	}

	public void nextChannel() {
		state.nextChannel();
	}

	public void prevChannel() {
		state.prevChannel();
	}

	public void turnUp() {
		state.turnUp();
	}

	public void turnDown() {
		state.turnDown();
	}

}
