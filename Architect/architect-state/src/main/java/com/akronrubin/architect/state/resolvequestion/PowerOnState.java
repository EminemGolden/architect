package com.akronrubin.architect.state.resolvequestion;

/**
 * 开机状态
 * 
 * @author wangdenghui
 *
 */
public class PowerOnState implements ITVState {

	@Override
	public void nextChannel() {
		// TODO Auto-generated method stub
		System.out.println("换下一个台操作成功");
	}

	@Override
	public void prevChannel() {
		// TODO Auto-generated method stub
		System.out.println("换上一个台操作成功");
	}

	@Override
	public void turnUp() {
		System.out.println("增加声音操作成功");
	}

	@Override
	public void turnDown() {
		// TODO Auto-generated method stub
		System.out.println("减小声音操作成功");
	}

}
