package com.akronrubin.architect.state.resolvequestion;

/**
 * 关闭电视的状态
 * 
 * @author wangdenghui
 *
 */
public class PowerOffState implements ITVState {

	@Override
	public void nextChannel() {
		// TODO Auto-generated method stub
		System.out.println("关机状态，无效操作");
	}

	@Override
	public void prevChannel() {
		// TODO Auto-generated method stub
		System.out.println("关机状态，无效操作");
	}

	@Override
	public void turnUp() {
		// TODO Auto-generated method stub
		System.out.println("关机状态，无效操作");
	}

	@Override
	public void turnDown() {
		// TODO Auto-generated method stub
		System.out.println("关机状态，无效操作");
	}

}
