package com.akronrubin.architect.state.question;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		TVController c = new TVController();
		//开机
		c.powerOn();
		c.nextChannel();
		c.turnUp();
		//关机
		c.powerOff();
		//上一个频道
		c.prevChannel();
		c.turnDown();
	}
}
