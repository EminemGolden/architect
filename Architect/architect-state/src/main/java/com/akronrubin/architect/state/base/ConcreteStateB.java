package com.akronrubin.architect.state.base;

/**
 * 具体的状态B
 * 
 * @author wangdenghui
 *
 */
public class ConcreteStateB implements IState {

	@Override
	public void handler(String tag) {
		System.out.println("B" + tag);
	}

}
