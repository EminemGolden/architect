package com.akronrubin.architect.state.question;

/**
 * 电视遥控器，控制电视开机、关机、切换频道、调整音量
 * 
 * @author wangdenghui
 *
 */
public class TVController {

	private static final int STATE_POWER_ON = 1;
	private static final int STATE_POWER_OFF = 2;

	private int mState = STATE_POWER_OFF;

	/**
	 * 开机
	 */
	public void powerOn() {
		if (mState == STATE_POWER_OFF) {
			mState = STATE_POWER_ON;
			System.out.println("开机");
		}
	}

	/**
	 * 关机
	 */
	public void powerOff() {
		if (mState == STATE_POWER_ON) {
			mState = STATE_POWER_OFF;
			System.out.println("关机");
		}
	}

	/**
	 * 下一频道
	 */
	public void nextChannel() {
		if (mState == STATE_POWER_ON) {
			System.out.println("下一频道");
		} else {
			System.out.println("提示没有开机！");
		}
	}

	/**
	 * 上一频道
	 */
	public void prevChannel() {
		if (mState == STATE_POWER_ON) {
			System.out.println("上一频道");
		} else {
			System.out.println("提示没有开机！");
		}
	}

	/**
	 * 调高音量
	 */
	public void turnUp() {
		if (mState == STATE_POWER_ON) {
			System.out.println("调高音量");
		} else {
			System.out.println("提示没有开机！");
		}
	}

	/**
	 * 调低音量
	 */
	public void turnDown() {
		if (mState == STATE_POWER_ON) {
			System.out.println("调低音量");
		} else {
			System.out.println("提示没有开机！");
		}
	}

}
