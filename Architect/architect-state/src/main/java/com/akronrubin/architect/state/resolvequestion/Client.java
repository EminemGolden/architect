package com.akronrubin.architect.state.resolvequestion;
import org.junit.Test;
public class Client {
	@Test
	public void main() {
		TVController controller = new TVController();
		controller.nextChannel();
		
		controller.powerOn();
		controller.nextChannel();
		controller.prevChannel();
		controller.turnUp();
		controller.turnDown();
		controller.powerOff();
		controller.nextChannel();
		
		
	}
}
