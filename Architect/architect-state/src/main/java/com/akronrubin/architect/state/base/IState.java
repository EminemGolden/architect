package com.akronrubin.architect.state.base;

/**
 * 状态接口
 * 
 * @author wangdenghui
 *
 */
public interface IState {

	void handler(String state);
}
