package com.akronrubin.architec_bridge.bridge.base;

/**
 * 抽象部分
 * 
 * @author wangdenghui
 *
 */
public abstract class Abstraction {
	// 持有实现部分的引用
	private Implementor impl;

	public Abstraction(Implementor impl) {
		super();
		this.impl = impl;
	}

	/**
	 * 通过调用实现部分具体的方法实现具体的功能
	 */
	public void operation() {
		impl.operationImpl();
	}

}
