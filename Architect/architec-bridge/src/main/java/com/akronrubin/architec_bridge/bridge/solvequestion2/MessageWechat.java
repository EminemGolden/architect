package com.akronrubin.architec_bridge.bridge.solvequestion2;

/**
 * 微信消息发送方式
 * @author wangdenghui
 *
 */
public class MessageWechat implements MessageSendWayImplementor{

	@Override
	public void sengMessage(String message, String to) {
		System.out.println("send wechat message "+message +" to "+to);
	}

}
