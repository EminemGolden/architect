package com.akronrubin.architec_bridge.bridge.solvequestion2;
/**
 * 消息类型抽象类
 * 消息类型：普通、加急、特急；
 * @author wangdenghui
 *
 */
public abstract class AbstractMessageType {

	/**
	 * 持有主动操作者
	 */
	private MessageSendWayImplementor implementor;

	public AbstractMessageType(MessageSendWayImplementor implementor) {
		super();
		this.implementor = implementor;
	}
	/**
	 * 消息调用消息操作者去实现发送某个类别的消息实体
	 * @param message
	 * @param to
	 */
	public void sendMessage(String message,String to){
		System.out.println(getMessageType());
		implementor.sengMessage(message, to);
	}
	
	protected abstract String getMessageType();
	
}
