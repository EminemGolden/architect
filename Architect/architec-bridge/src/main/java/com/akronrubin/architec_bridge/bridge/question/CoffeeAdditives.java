package com.akronrubin.architec_bridge.bridge.question;

/**
 * 咖啡能加的东西（实现部分）
 * @author Jason
 * QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public abstract class CoffeeAdditives {

	/**
	 * 添加方法
	 * @return
	 */
	public abstract String add();
	
}
