package com.akronrubin.architec_bridge.bridge.solvequestion2;

/**
 * 普通级别消息
 * 
 * @author wangdenghui
 *
 */
public class CommonMessage extends AbstractMessageType{

	public CommonMessage(MessageSendWayImplementor implementor) {
		super(implementor);
	}

	@Override
	protected String getMessageType() {
		// TODO Auto-generated method stub
		return "普通消息：";
	}

}
