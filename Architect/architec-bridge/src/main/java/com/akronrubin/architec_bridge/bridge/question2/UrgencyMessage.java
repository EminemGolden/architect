package com.akronrubin.architec_bridge.bridge.question2;

/**
 * 加急消息的抽象接口
 * @author Jason
 * QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public interface UrgencyMessage extends Message{

	/**
	 * 监控消息的处理（区分普通消息）
	 * @param messageId 消息编号
	 */
	void watch(String messageId);
	
}
