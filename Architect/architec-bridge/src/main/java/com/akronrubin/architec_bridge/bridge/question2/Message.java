package com.akronrubin.architec_bridge.bridge.question2;

/**
 * 消息的统一接口
 * @author Jason
 * QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public interface Message {

	/**
	 * 发送消息
	 * @param message 要发送的消息内容
	 * @param to 消息的接收者
	 */
	void send(String message, String to);
	
}
