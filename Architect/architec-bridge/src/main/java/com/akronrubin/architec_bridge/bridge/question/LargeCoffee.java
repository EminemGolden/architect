package com.akronrubin.architec_bridge.bridge.question;

/**
 * 大杯咖啡（抽象部分的具体实现）
 * @author Jason
 * QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public class LargeCoffee extends Coffee{

	public LargeCoffee(CoffeeAdditives impl) {
		super(impl);
	}

	@Override
	public void makeCoffee() {
		System.out.println("大杯的"+impl.add()+"咖啡");
	}

}
