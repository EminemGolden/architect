package com.akronrubin.architec_bridge.bridge.solvequestion2;

/**
 * 消息发送方式抽象接口 ，负责消息发送
 * 消息发送方式：站内消息、邮件、手机短信；
 * 
 * @author wangdenghui
 *
 */
public interface MessageSendWayImplementor {

	void sengMessage(String message, String to);

}
