package com.akronrubin.architec_bridge.bridge.question;

/**
 * 抽象的咖啡（抽象部分）
 * 
 * @author Jason QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public abstract class Coffee {

	// 持有实现部分的引用
	protected CoffeeAdditives impl;

	public Coffee(CoffeeAdditives impl) {
		super();
		this.impl = impl;
	}

	/**
	 * 咖啡具体做成了什么样子由子类决定
	 */
	public abstract void makeCoffee();
}
