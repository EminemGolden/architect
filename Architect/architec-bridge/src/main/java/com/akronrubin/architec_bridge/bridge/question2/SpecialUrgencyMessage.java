package com.akronrubin.architec_bridge.bridge.question2;

/**
 * 特急消息
 * @author Jason
 * QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public interface SpecialUrgencyMessage extends Message{

	/**
	 * 加急
	 */
	void hurry();
}
