package com.akronrubin.architec_bridge.bridge.question2;

/**
 * 普通的站内短消息
 * @author Jason
 * QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public class CommonMessageSMS implements Message{

	@Override
	public void send(String message, String to) {
		System.out.println("发送普通站内短消息："+message+"，给"+to);
	}

}
