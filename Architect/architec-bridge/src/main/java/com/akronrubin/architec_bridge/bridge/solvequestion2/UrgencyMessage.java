package com.akronrubin.architec_bridge.bridge.solvequestion2;

/**
 * 加急消息
 * 
 * @author wangdenghui
 *
 */
public class UrgencyMessage extends AbstractMessageType {

	public UrgencyMessage(MessageSendWayImplementor implementor) {
		super(implementor);
	}

	@Override
	protected String getMessageType() {
		// TODO Auto-generated method stub
		return "紧急信息：";
	}

}
