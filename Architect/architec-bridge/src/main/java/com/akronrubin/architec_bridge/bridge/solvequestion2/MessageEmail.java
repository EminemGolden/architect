package com.akronrubin.architec_bridge.bridge.solvequestion2;

/**
 * 邮件发送方式
 * 
 * @author wangdenghui
 *
 */
public class MessageEmail implements MessageSendWayImplementor {

	@Override
	public void sengMessage(String message, String to) {
		System.out.println("send email message " + message + ",to " + to);
	}

}
