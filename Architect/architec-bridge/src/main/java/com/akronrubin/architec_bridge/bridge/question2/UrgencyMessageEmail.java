package com.akronrubin.architec_bridge.bridge.question2;

/**
 * 加急邮件消息
 * @author Jason
 * QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public class UrgencyMessageEmail implements UrgencyMessage{

	@Override
	public void watch(String messageId) {
		//
	}

	@Override
	public void send(String message, String to) {
		System.out.println("发送加急邮件消息："+message+"，给"+to);
	}

}
