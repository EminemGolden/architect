package com.akronrubin.architec_bridge.bridge.question;

/**
 * 奶
 * @author Jason
 * QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public class Milk extends CoffeeAdditives {

	@Override
	public String add() {
		return "加奶";
	}

}
