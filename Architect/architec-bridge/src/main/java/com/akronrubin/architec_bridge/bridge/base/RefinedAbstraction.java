package com.akronrubin.architec_bridge.bridge.base;

/**
 * 优化的抽象部分
 * 
 * @author wangdenghui
 *
 */
public class RefinedAbstraction extends Abstraction {

	public RefinedAbstraction(Implementor impl) {
		super(impl);
	}

	public void refinedOperation() {
		// 对Abstraction 中的方法扩展
	}

}
