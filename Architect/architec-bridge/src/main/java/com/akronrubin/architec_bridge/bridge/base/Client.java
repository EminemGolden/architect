package com.akronrubin.architec_bridge.bridge.base;

import org.junit.Test;

public class Client {

    @Test
    public void main() {
        Abstraction abstraction = new RefinedAbstraction(new ConcreateImplementorA());
        abstraction.operation();
    }

}
