package com.akronrubin.architec_bridge.bridge.solvequestion2;

import org.junit.Test;

/**
 * 本次案例需求：组合发送方式＋消息类别，用特定方式发送特定类型的随机组合类别消息；
 * 消息发送方式：站内消息、邮件、手机短信（消息发送的手段）；
 * 消息类别：普通、加急、特急（消息的类型）；
 * （普通短信、普通邮件、加急短信、加急邮件），消息发送的手段去发送消息
 * @author wangdenghui
 *
 */
public class Client {

	@Test
	public void main() {
		
//		AbstractMessageType message = new SpecialUrgencyMessage(new MessageWechat());
//		AbstractMessageType message  = new UrgencyMessage(new MessageSMS());
		AbstractMessageType message  = new CommonMessage(new MessageSMS());
		message.sendMessage("骑士夺得16年总冠军", "摇头丸");
		
	}

}
