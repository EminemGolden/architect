package com.akronrubin.architec_bridge.bridge.base;

public interface Implementor {

	void operationImpl();
}
