package com.akronrubin.architec_bridge.bridge.question2;

/**
 * 特级站内消息
 * @author Jason
 * QQ: 1476949583
 * @date 2016年2月24日
 * @version 1.0
 */
public class SpecialUrgencyMessageSMS implements SpecialUrgencyMessage {

	@Override
	public void send(String message, String to) {
		System.out.println("发送特级站内消息："+message+"，给"+to);
	}

	@Override
	public void hurry() {
		
	}

}
