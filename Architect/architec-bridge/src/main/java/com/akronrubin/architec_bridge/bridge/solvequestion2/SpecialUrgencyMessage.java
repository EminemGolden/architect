package com.akronrubin.architec_bridge.bridge.solvequestion2;

/**
 * 特急消息
 * 
 * @author wangdenghui
 *
 */
public class SpecialUrgencyMessage extends AbstractMessageType {

	public SpecialUrgencyMessage(MessageSendWayImplementor implementor) {
		super(implementor);
	}

	@Override
	protected String getMessageType() {
		return "特急消息：";
	}

}
