package com.akronrubin.architec_bridge.bridge.solvequestion2;

/**
 * 短信消息
 * 
 * @author wangdenghui
 *
 */
public class MessageSMS implements MessageSendWayImplementor {

	@Override
	public void sengMessage(String message, String to) {
		System.out.println("send SMS message " + message + " to " + to);
	}

}
