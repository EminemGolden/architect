package com.akronrubin.architec_bridge.bridge.question;

import org.junit.Test;

public class Client {

	@Test
	public void main() {
		//糖
		Sugar sugar = new Sugar();
		//原味
		Ordinary ordinary = new Ordinary();
		//奶
		Milk milk = new Milk();
		
		//大杯
		LargeCoffee c1 = new LargeCoffee(ordinary);
		c1.makeCoffee();
		
		//小杯
		SmallCoffee c2 = new SmallCoffee(sugar);
		c2.makeCoffee();
		
		//大杯加奶咖啡
		LargeCoffee c3 = new LargeCoffee(milk);
		c3.makeCoffee();
	}

}
