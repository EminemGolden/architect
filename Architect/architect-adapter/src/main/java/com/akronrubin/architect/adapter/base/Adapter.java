package com.akronrubin.architect.adapter.base;

public class Adapter implements Target {

	private Adaptee adaptee;
	
	public Adapter(Adaptee adaptee) {
		super();
		this.adaptee = adaptee;
	}


	@Override
	public void request() {
		if(null != adaptee){
			adaptee.specificRequest();
		}
	}

}
