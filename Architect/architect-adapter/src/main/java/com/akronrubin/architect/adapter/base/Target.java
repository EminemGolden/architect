package com.akronrubin.architect.adapter.base;

public interface Target {

	void request();
}
