package com.akronrubin.architect.adapter.base;

import org.junit.Test;

public class Client {

	@Test
	public void main() {
		Target target = new Adapter(new AdapteeImple());
		target.request();
	}
}
