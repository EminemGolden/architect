package com.akronrubin.architect.adapter.base;

public interface Adaptee {

	void specificRequest();
}
