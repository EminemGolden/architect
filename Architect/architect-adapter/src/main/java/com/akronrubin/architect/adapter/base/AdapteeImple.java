package com.akronrubin.architect.adapter.base;

public class AdapteeImple implements Adaptee{

	@Override
	public void specificRequest() {
		System.out.println("需要适配的特殊类型");
	}

}
