package com.akronrubin.architect.flyweight.base;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 享元工场，管理所有享元对象的缓存池
 * 
 * @author wangdenghui
 *
 */
public class FlyWeightFactory {

	private Map<String, FlyWeight> map = new ConcurrentHashMap<String, FlyWeight>();

	/**
	 * 获取key对应的享元对象
	 * 
	 * @param key
	 * @return
	 */
	public FlyWeight get(String key) {
		FlyWeight value = map.get(key);
		if (value == null) {
			// key 是不受外部干扰的属性
			value = new ConcreteFlyWeight(key);
			map.put(key, value);
			return value;
		}
		return value;
	}

}
