package com.akronrubin.architect.flyweight.demo;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		
		TicketFactory factory = new TicketFactory();
		Ticket t1 = factory.getTicket("北京", "深圳");
		t1.showTicket("软卧");
		
		Ticket t2 = factory.getTicket("北京", "深圳");
		t2.showTicket("硬座");
		
		Ticket t3 = factory.getTicket("深圳", "上海");
		t3.showTicket("站票");
		
		
		
	}

}
