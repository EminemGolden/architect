package com.akronrubin.architect.flyweight.demo;

import java.util.Random;

/**
 * 享元具体实现类
 * 
 * @author wangdenghui
 *
 */
public class TrainTicket implements Ticket {

	/**
	 * 火车票对象里面只有bunk（车票类型）是可变的，其他都是不变的， 
	 * 所以在外部改变bunk属性的状态，然后跟据一个不变的key来查找对应的对象
	 */
	private String from;
	private String to;
	private String bunk;
	private int price;

	public TrainTicket(String from, String to) {
		super();
		this.from = from;
		this.to = to;
	}

	@Override
	public void showTicket(String bunk) {
		price = new Random().nextInt(100);
		System.out.println(String.format("%s - %s的%s火车票，价格为%d", from, to, bunk, price));
	}

}
