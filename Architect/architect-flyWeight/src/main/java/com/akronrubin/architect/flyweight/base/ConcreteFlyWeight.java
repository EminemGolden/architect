package com.akronrubin.architect.flyweight.base;

public class ConcreteFlyWeight implements FlyWeight {

	private String instrinsicState;

	public ConcreteFlyWeight(String instrinsicState) {
		super();
		this.instrinsicState = instrinsicState;
	}

	@Override
	public void operation(String extrinsicState) {
		// 外部传来的被享元的属性，以此属性为表示可以改变被享元对象的其他属性
	}

}
