package com.akronrubin.architect.flyweight.base;

/**
 * 享元模式接口，需要共享的对象应该事先这个接口
 * 
 * @author wangdenghui
 *
 */
public interface FlyWeight {

	/**
	 * 传入外部状态，可以改变分离的属性
	 * 
	 * @param extrinsicState
	 *            外部传入的对象不变值
	 */
	void operation(String extrinsicState);

}
