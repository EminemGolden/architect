package com.akronrubin.architect.flyweight.base;
import org.junit.Test;
/**
 * 享元模式，针对享元对象的可变属性进行缓存操作
 * 
 * @author wangdenghui
 *
 */
public class Client {

	@Test
	public void main() {
		FlyWeightFactory factory = new FlyWeightFactory();

		FlyWeight f1 = factory.get("key1");
		FlyWeight f2 = factory.get("key2");
		FlyWeight f3 = factory.get("key2");

		System.out.println(f1 == f2);
		System.out.println(f3 == f2);
	}

}
