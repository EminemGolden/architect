package com.akronrubin.architect.flyweight.demo;

/**
 * 享元接口
 * 
 * @author wangdenghui
 *
 */
public interface Ticket {

	void showTicket(String bunk);

}
