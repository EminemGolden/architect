package com.akronrubin.architect.flyweight.demo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 管理对象的缓存manager
 * 
 * @author wangdenghui
 *
 */
public class TicketFactory {

	private Map<String, Ticket> tickets = new ConcurrentHashMap<String, Ticket>();

	public Ticket getTicket(String from, String to) {
		// 以from + "-" + to组成不变对象来作为对象池的缓存key
		String key = from + "-" + to;
		if (tickets.containsKey(key)) {
			System.out.println("缓存池内发现有缓存，获取缓存对象..");
			return tickets.get(key);
		} else {
			System.out.println("对象池内没有缓存对象，创建新对象...");
			Ticket ticket = new TrainTicket(from, to);
			tickets.put(key, ticket);
			return ticket;
		}
	}

}
