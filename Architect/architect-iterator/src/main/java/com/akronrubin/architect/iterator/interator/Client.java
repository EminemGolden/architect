package com.akronrubin.architect.iterator.interator;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		//容器
		Aggregate<String> collection = new ConcreteAggregate<String>();
		collection.add("湖南卫视");
		collection.add("浙江卫视");
		collection.add("江苏卫视");
		collection.add("东方卫视");
		
		//遍历
		Iterator<String> iterator = collection.iterator();
		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
	}

}
