package com.akronrubin.architect.builder.builder.exportdata;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 导出数据到文本文件
 * 
 * @author EminemGolden
 *
 */
public class ExportToTxt {

	/**
	 * 导出
	 * 
	 * @param ehm
	 *            文件头
	 * @param mapData
	 *            数据
	 * @param efm
	 *            文件尾
	 * @return
	 */
	public StringBuffer export(ExportHeaderModel ehm, Map<String, Collection<ExportDataModel>> mapData,
			ExportFooterModel efm) {
		StringBuffer buffer = new StringBuffer();
		// 拼接头
		buffer.append(ehm.getDepId() + "," + ehm.getExportDate()).append("\n");
		// 数据
		Set<Entry<String, Collection<ExportDataModel>>> entrySet = mapData.entrySet();
		for (Entry<String, Collection<ExportDataModel>> entry : entrySet) {
			buffer.append(entry.getKey()).append("\n");
			for (ExportDataModel edm : entry.getValue()) {
				buffer.append(edm.getProductId() + "," + edm.getPrice() + "," + edm.getAmount()).append("\n");
			}
		}
		// 拼接尾
		buffer.append(efm.getExportUser());

		return buffer;
	}

}
