package com.akronrubin.architect.builder.builder.base3;

/**
 * 保险合同 这些属性属于保险合同对象的部件 (需要被创建的实体类)
 * 
 * @author EminemGolden
 *
 */
public class InsuranceContract {

	/**
	 * 保险合同编号
	 */
	private String contractId;

	/**
	 * 被保险人（被保险人和被保险公司，这个两个属性不能同时有值）
	 */
	private String personName;

	/**
	 * 被保险公司
	 */
	private String companyName;

	/**
	 * 保险生效的日期
	 */
	private String beginDate;

	/**
	 * 保险失效的日期
	 */
	private String endDate;

	// 默认（统一构建过程），只有同包访问
	// 强制开发者使用构建起创建合同对象
	private InsuranceContract(String contractId, String personName, String companyName, String beginDate,
			String endDate) {
		super();
		this.contractId = contractId;
		this.personName = personName;
		this.companyName = companyName;
		this.beginDate = beginDate;
		this.endDate = endDate;
	}

	public void operations() {
		System.out.println("保险合同的某些操作...");
	}

	@Override
	public String toString() {
		return "InsuranceContract [contractId=" + contractId + ", personName=" + personName + ", companyName="
				+ companyName + ", beginDate=" + beginDate + ", endDate=" + endDate + "]";
	}

	/**
	 * 将需要被创建的实体类对象的属性引入到构建控件里面，实现链式编程
	 *   声明为静态内部类   保证类的安全
	 * @author EminemGolden
	 *
	 */
	public static class Builder {

		/**
		 * 保险合同编号
		 */
		private String contractId;

		/**
		 * 被保险人（被保险人和被保险公司，这个两个属性不能同时有值）
		 */
		private String personName;

		/**
		 * 被保险公司
		 */
		private String companyName;

		/**
		 * 保险生效的日期
		 */
		private String beginDate;

		/**
		 * 保险失效的日期
		 */
		private String endDate;

		public Builder() {
		}

		public Builder(String contractId, String beginDate, String endDate) {
			this.contractId = contractId;
			this.beginDate = beginDate;
			this.endDate = endDate;
		}

		public Builder setContractId(String contractId) {
			this.contractId = contractId;
			return this;
		}

		public Builder setPersonName(String personName) {
			this.personName = personName;
			return this;
		}

		public Builder setCompanyName(String companyName) {
			this.companyName = companyName;
			return this;
		}

		public Builder setBeginDate(String beginDate) {
			this.beginDate = beginDate;
			return this;
		}

		public Builder setEndDate(String endDate) {
			this.endDate = endDate;
			return this;
		}

		public InsuranceContract builder() {
			try {
				boolean contractIdFlag = contractId != null && contractId.trim().length() == 0;
				if (contractIdFlag) {
					throw new Exception("保险合同编号不能为空");
				}
				boolean beginDateFlag = beginDate != null && beginDate.trim().length() == 0;
				if (beginDateFlag) {
					throw new Exception("保险合同不能没有开始签署时间");
				}
				boolean endDateFlag = endDate != null && endDate.trim().length() == 0;
				if (endDateFlag) {
					throw new Exception("保险合同不能没有结束签署时间");
				}

				boolean personNameFlag = personName != null && personName.trim().length() > 0;
				boolean companyNameFlag = companyName != null && companyName.trim().length() > 0;
				if (personNameFlag && companyNameFlag) {
					throw new Exception("一份保险合同不能既和人签署又和公司签署");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return new InsuranceContract(contractId, personName, companyName, beginDate, endDate);
		}

	}
}
