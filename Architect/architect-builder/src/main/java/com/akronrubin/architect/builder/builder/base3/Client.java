package com.akronrubin.architect.builder.builder.base3;

import org.junit.Test;

/**
 * Builder封装为静态内部类的方式实现构造器模式
 * 
 * @author EminemGolden
 *
 */
public class Client {

	@Test
	public void main() {
		InsuranceContract.Builder builder = new InsuranceContract.Builder();
		builder.setContractId("A001").setBeginDate("2009-09-7").setEndDate("2088-9-12").setPersonName("羊咩咩")
				.setCompanyName("谷歌公司");
		System.out.println(builder.builder());
	}
}
