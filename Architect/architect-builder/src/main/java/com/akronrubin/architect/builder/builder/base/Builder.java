package com.akronrubin.architect.builder.builder.base;

/**
 * 生成器接口，定义创建一个Product对象所需的各个部件的操作
 * 
 * @author EminemGolden
 *
 */
public interface Builder {

	void buildPart();
}
