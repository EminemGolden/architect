package com.akronrubin.architect.builder.builder.base;

/**
 * 指导者，主要来使用Builder接口，以一个统一的过程来构建所需要的Product对象
 * 
 * @author EminemGolden
 *
 */
public class Director {

	private Builder builder;

	public Director(Builder builder) {
		super();
		this.builder = builder;
	}

	public void construct() {
		builder.buildPart();
	}

}
