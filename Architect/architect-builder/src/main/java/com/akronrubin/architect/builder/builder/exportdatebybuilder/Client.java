package com.akronrubin.architect.builder.builder.exportdatebybuilder;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Client {

	@Test
	public void main() {
		// 头
		ExportHeaderModel ehm = new ExportHeaderModel();
		ehm.setDepId("上海分公司");
		ehm.setExportDate("2015-12-11");

		// 数据
		Map<String, Collection<ExportDataModel>> mapData = new HashMap<String, Collection<ExportDataModel>>();
		Collection<ExportDataModel> co_1 = new ArrayList<ExportDataModel>();

		ExportDataModel edm = new ExportDataModel();
		edm.setProductId("产品001");
		edm.setPrice(100);
		edm.setAmount(80);
		co_1.add(edm);

		ExportDataModel edm2 = new ExportDataModel();
		edm2.setProductId("产品002");
		edm2.setPrice(99);
		edm2.setAmount(56);
		co_1.add(edm2);

		mapData.put("销售表", co_1);

		Collection<ExportDataModel> co_2 = new ArrayList<ExportDataModel>();

		ExportDataModel edm3 = new ExportDataModel();
		edm3.setProductId("产品001");
		edm3.setPrice(100);
		edm3.setAmount(80);
		co_2.add(edm3);

		ExportDataModel edm4 = new ExportDataModel();
		edm4.setProductId("产品002");
		edm4.setPrice(99);
		edm4.setAmount(56);
		co_2.add(edm4);

		mapData.put("采购表", co_2);

		// 尾
		ExportFooterModel efm = new ExportFooterModel();
		efm.setExportUser("Eminem");

		Director director = new Director(new XmlBuilder());
		System.out.println(director.construct(ehm, mapData, efm));
	}

}
