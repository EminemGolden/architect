package com.akronrubin.architect.builder.builder.exportdata;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

/**
 * 导出数据到XML文件
 * 
 * @author EminemGolden
 *
 */
public class ExportToXml {

	/**
	 * 导出
	 * 
	 * @param ehm
	 *            文件头
	 * @param mapData
	 *            数据
	 * @param efm
	 *            文件尾
	 * @return
	 */
	public StringBuffer export(ExportHeaderModel ehm, Map<String, Collection<ExportDataModel>> mapData,
			ExportFooterModel efm) {
		StringBuffer buffer = new StringBuffer();
		// 拼接头部
		buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		buffer.append("<Report>\n");
		buffer.append("	<Header>\n");
		buffer.append("		<DepId>").append(ehm.getDepId()).append("</DepId>\n");
		buffer.append("		<ExportDate>").append(ehm.getExportDate()).append("</ExportDate>\n");
		buffer.append("	</Header>\n");
		// 数据内容
		buffer.append("	<Body>\n");
		Set<Entry<String, Collection<ExportDataModel>>> entrySet = mapData.entrySet();
		for (Entry<String, Collection<ExportDataModel>> entry : entrySet) {
			buffer.append("		<Datas TableName=\"" + entry.getKey() + "\">\n");
			for (ExportDataModel edm : entry.getValue()) {
				buffer.append(" 	<Data>\n");
				buffer.append("			<ProductId>").append(edm.getProductId()).append("</ProductId>\n");
				buffer.append("			<Price>").append(edm.getPrice()).append("</Price>\n");
				buffer.append("			<Amount>").append(edm.getAmount()).append("</Amount>\n");
				buffer.append(" 	</Data>\n");
			}
			buffer.append("		</Datas>\n");
		}

		buffer.append("	</Body>\n");

		// 尾部
		buffer.append("	<Footer>\n");
		buffer.append("		<ExportUser>").append(efm.getExportUser()).append("</ExportUser>\n");
		buffer.append("	</Footer>\n");

		buffer.append("</Report>\n");

		return buffer;
	}

}
