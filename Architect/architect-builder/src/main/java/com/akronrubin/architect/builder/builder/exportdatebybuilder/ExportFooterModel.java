package com.akronrubin.architect.builder.builder.exportdatebybuilder;

/**
 * 文件尾
 * 
 * @author EminemGolden
 *
 */
public class ExportFooterModel {

	/**
	 * 输出人
	 */
	private String exportUser;

	public String getExportUser() {
		return exportUser;
	}

	public void setExportUser(String exportUser) {
		this.exportUser = exportUser;
	}

}
