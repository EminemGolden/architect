package com.akronrubin.architect.builder.builder.base;

/**
 * 生成器的具体实现
 * 
 * @author EminemGolden
 *
 */
public class ConcreteBuilder implements Builder {

	private Product product;

	@Override
	public void buildPart() {

	}

	public Product getResult() {
		return product;
	}

}
