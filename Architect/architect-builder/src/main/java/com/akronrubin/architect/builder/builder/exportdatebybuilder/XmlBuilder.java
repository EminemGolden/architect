package com.akronrubin.architect.builder.builder.exportdatebybuilder;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

/**
 * 导出数据到XML文件
 * 
 * @author EminemGolden
 *
 */
public class XmlBuilder implements Builder {

	private StringBuffer buffer = new StringBuffer();

	@Override
	public void buildHeader(ExportHeaderModel ehm) {
		buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		buffer.append("<Report>\n");
		buffer.append("	<Header>\n");
		buffer.append("		<DepId>").append(ehm.getDepId()).append("</DepId>\n");
		buffer.append("		<ExportDate>").append(ehm.getExportDate()).append("</ExportDate>\n");
		buffer.append("	</Header>\n");
	}

	@Override
	public void buildBody(Map<String, Collection<ExportDataModel>> mapData) {
		// 数据内容
		buffer.append("	<Body>\n");
		Set<Entry<String, Collection<ExportDataModel>>> entrySet = mapData.entrySet();
		for (Entry<String, Collection<ExportDataModel>> entry : entrySet) {
			buffer.append("		<Datas TableName=\"" + entry.getKey() + "\">\n");
			for (ExportDataModel edm : entry.getValue()) {
				buffer.append(" 	<Data>\n");
				buffer.append("			<ProductId>").append(edm.getProductId()).append("</ProductId>\n");
				buffer.append("			<Price>").append(edm.getPrice()).append("</Price>\n");
				buffer.append("			<Amount>").append(edm.getAmount()).append("</Amount>\n");
				buffer.append(" 	</Data>\n");
			}
			buffer.append("		</Datas>\n");
		}
		buffer.append("	</Body>\n");
	}

	@Override
	public void buildFooter(ExportFooterModel efm) {
		// 尾部
		buffer.append("	<Footer>\n");
		buffer.append("		<ExportUser>").append(efm.getExportUser()).append("</ExportUser>\n");
		buffer.append("	</Footer>\n");
		buffer.append("</Report>\n");
	}

	@Override
	public StringBuffer getResult() {
		return buffer;
	}

}
