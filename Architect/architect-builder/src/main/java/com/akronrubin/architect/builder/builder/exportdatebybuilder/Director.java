package com.akronrubin.architect.builder.builder.exportdatebybuilder;

import java.util.Collection;
import java.util.Map;

/**
 * 负责将Builder创建出来的部位组装为产品
 * 
 * @author EminemGolden
 *
 */
public class Director {

	private Builder builder;

	public Director(Builder builder) {
		super();
		this.builder = builder;
	}

	public StringBuffer construct(ExportHeaderModel ehm, Map<String, Collection<ExportDataModel>> mapData,
			ExportFooterModel efm) {
		builder.buildHeader(ehm);
		builder.buildBody(mapData);
		builder.buildFooter(efm);
		return builder.getResult();
	}

}
