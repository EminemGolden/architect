package com.akronrubin.architect.builder.builder.exportdatebybuilder;

/**
 * 描述输出数据的对象
 * 
 * @author EminemGolden
 *
 */
public class ExportDataModel {

	/**
	 * 产品ID
	 */
	private String productId;

	/**
	 * 产品价格
	 */
	private double price;

	/**
	 * 产品数量
	 */
	private double amount;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
