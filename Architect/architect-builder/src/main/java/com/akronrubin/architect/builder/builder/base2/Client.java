package com.akronrubin.architect.builder.builder.base2;

import org.junit.Test;

public class Client {

	@Test
	public void main() {
		ConcreteBuilder builder = new ConcreteBuilder();
		builder
		.setContractId("A001")
		.setBeginDate("2009-09-7")
		.setEndDate("2088-9-12")
//		.setPersonName("羊咩咩")
		.setCompanyName("谷歌公司");
		System.out.println(builder.builder());
	}
}
