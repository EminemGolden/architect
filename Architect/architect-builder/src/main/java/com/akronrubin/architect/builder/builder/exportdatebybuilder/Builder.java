package com.akronrubin.architect.builder.builder.exportdatebybuilder;

import java.util.Collection;
import java.util.Map;

/**
 * 生成器接口，定义创建一个输出文件对象所需的各个部件的操作
 *  创建产品的部位
 * @author EminemGolden
 *
 */
public interface Builder {

	/**
	 * 构建头部
	 * 
	 * @param ehm
	 */
	void buildHeader(ExportHeaderModel ehm);

	/**
	 * 构建数据
	 * 
	 * @param mapData
	 */
	void buildBody(Map<String, Collection<ExportDataModel>> mapData);

	/**
	 * 构建尾部
	 * 
	 * @param efm
	 */
	void buildFooter(ExportFooterModel efm);

	StringBuffer getResult();
}
