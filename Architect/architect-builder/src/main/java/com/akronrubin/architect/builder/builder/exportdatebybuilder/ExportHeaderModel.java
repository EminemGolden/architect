package com.akronrubin.architect.builder.builder.exportdatebybuilder;

/**
 * 导出的文件头
 * 
 * @author EminemGolden
 *
 */
public class ExportHeaderModel {

	/**
	 * 部门编号
	 */
	private String depId;

	/**
	 * 导出日期
	 */
	private String exportDate;

	public String getDepId() {
		return depId;
	}

	public void setDepId(String depId) {
		this.depId = depId;
	}

	public String getExportDate() {
		return exportDate;
	}

	public void setExportDate(String exportDate) {
		this.exportDate = exportDate;
	}

}
