package com.akronrubin.architect.builder.builder.base2;

/**
 * 保险合同 这些属性属于保险合同对象的部件 (需要被创建的实体类)
 * 
 * @author EminemGolden
 *
 */
public class InsuranceContract {

	/**
	 * 保险合同编号
	 */
	private String contractId;

	/**
	 * 被保险人（被保险人和被保险公司，这个两个属性不能同时有值）
	 */
	private String personName;

	/**
	 * 被保险公司
	 */
	private String companyName;

	/**
	 * 保险生效的日期
	 */
	private String beginDate;

	/**
	 * 保险失效的日期
	 */
	private String endDate;

	// 默认（统一构建过程），只有同包访问
	// 强制开发者使用构建起创建合同对象
	InsuranceContract(String contractId, String personName, String companyName, String beginDate, String endDate) {
		super();
		this.contractId = contractId;
		this.personName = personName;
		this.companyName = companyName;
		this.beginDate = beginDate;
		this.endDate = endDate;
	}

	public void operations() {
		System.out.println("保险合同的某些操作...");
	}

	@Override
	public String toString() {
		return "InsuranceContract [contractId=" + contractId + ", personName=" + personName + ", companyName="
				+ companyName + ", beginDate=" + beginDate + ", endDate=" + endDate + "]";
	}

}
