package com.akronrubin.architect.builder.builder.base;

/**
 * 产品，表示被生成器构建的复杂对象，包含多个部件
 * 
 * @author EminemGolden
 *
 */
public interface Product {

}
