package com.akronrubin.architect.builder.builder.exportdatebybuilder;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 导出数据到文本文件
 * 
 * @author EminemGolden
 *
 */
public class TextBuilder implements Builder {

	private StringBuffer buffer = new StringBuffer();

	/**
	 * 导出
	 * 
	 * @param ehm
	 *            文件头
	 * @param mapData
	 *            数据
	 * @param efm
	 *            文件尾
	 * @return
	 */
	public StringBuffer export(ExportHeaderModel ehm, Map<String, Collection<ExportDataModel>> mapData,
			ExportFooterModel efm) {
		StringBuffer buffer = new StringBuffer();
		// 拼接头
		// 数据
		Set<Entry<String, Collection<ExportDataModel>>> entrySet = mapData.entrySet();
		for (Entry<String, Collection<ExportDataModel>> entry : entrySet) {
			buffer.append(entry.getKey()).append("\n");
			for (ExportDataModel edm : entry.getValue()) {
				buffer.append(edm.getProductId() + "," + edm.getPrice() + "," + edm.getAmount()).append("\n");
			}
		}
		// 拼接尾
		buffer.append(efm.getExportUser());

		return buffer;
	}

	@Override
	public void buildHeader(ExportHeaderModel ehm) {
		// 拼接头
		buffer.append(ehm.getDepId() + "," + ehm.getExportDate()).append("\n");
	}

	@Override
	public void buildBody(Map<String, Collection<ExportDataModel>> mapData) {
		Set<Entry<String, Collection<ExportDataModel>>> entrySet = mapData.entrySet();
		for (Entry<String, Collection<ExportDataModel>> entry : entrySet) {
			buffer.append(entry.getKey()).append("\n");
			for (ExportDataModel edm : entry.getValue()) {
				buffer.append(edm.getProductId() + "," + edm.getPrice() + "," + edm.getAmount()).append("\n");
			}
		}
	}

	@Override
	public void buildFooter(ExportFooterModel efm) {
		buffer.append(efm.getExportUser());
	}

	@Override
	public StringBuffer getResult() {
		return buffer;
	}

}
