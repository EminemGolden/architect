package com.akronrubin.architect.facade.base;

/**
 * 外观模式
 * 
 * @author EminemGolden
 *
 */
public class Facade {
	public void generate() {
		BizModuleGen bmg = new BizModuleGen();
		bmg.generate();

		DaoModuleGen dmg = new DaoModuleGen();
		dmg.generate();

		ModelModuleGen mmg = new ModelModuleGen();
		mmg.generate();
	}

}
