package com.akronrubin.architect.facade.base;
import org.junit.Test;
/**
 * 外观模式
 * 
 * @author EminemGolden
 *
 */
public class Client {

	@Test
	public void main() {
		Facade  facade = new Facade();
		facade.generate();
	}

}
