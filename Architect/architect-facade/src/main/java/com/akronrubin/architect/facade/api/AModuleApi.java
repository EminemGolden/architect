package com.akronrubin.architect.facade.api;

public interface AModuleApi {

	void operate();
}
