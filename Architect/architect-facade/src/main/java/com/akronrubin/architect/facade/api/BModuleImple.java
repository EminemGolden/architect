package com.akronrubin.architect.facade.api;

public class BModuleImple implements BModuleApi {

	@Override
	public void operate() {
		System.out.println("B 模块实现");
	}

}
