package com.akronrubin.architect.facade.api;

public class AModuleImple implements AModuleApi {

	@Override
	public void operate() {
		System.out.println("A 模块实现");
	}

}
