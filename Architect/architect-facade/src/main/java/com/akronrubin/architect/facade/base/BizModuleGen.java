package com.akronrubin.architect.facade.base;

/**
 * 生成业务代码
 * 
 * @author EminemGolden
 *
 */
public class BizModuleGen {

	public void generate() {
		System.out.println("生成一千个业务类...");
	}
}
