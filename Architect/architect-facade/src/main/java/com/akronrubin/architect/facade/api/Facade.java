package com.akronrubin.architect.facade.api;

/**
 * Facade结构，统一生成对应模块的界面，让Client不深入到模块内部获取数据域
 * @author EminemGolden
 *
 */
public class Facade {

	public void excute(){
		AModuleApi aModuleApi = new AModuleImple();
		aModuleApi.operate();
		
		BModuleApi bModuleApi = new BModuleImple();
		bModuleApi.operate();
	}
}
