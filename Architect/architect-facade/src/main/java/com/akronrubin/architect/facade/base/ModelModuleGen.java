package com.akronrubin.architect.facade.base;

/**
 * 生成实体类
 * 
 * @author EminemGolden
 *
 */
public class ModelModuleGen {

	public void generate() {
		System.out.println("生成一千个实体类...");
	}
}
