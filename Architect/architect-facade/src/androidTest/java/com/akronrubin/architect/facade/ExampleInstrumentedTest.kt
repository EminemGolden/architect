package com.akronrubin.architect.facade

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.akronrubin.architect.facade.api.Facade
import org.junit.Test
import org.junit.runner.RunWith

class ExampleInstrumentedTest {

    @Test
    fun useFacade() {
        val facade = Facade()
        facade.excute()
    }
}