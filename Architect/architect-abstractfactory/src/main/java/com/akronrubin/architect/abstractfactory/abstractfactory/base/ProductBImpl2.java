package com.akronrubin.architect.abstractfactory.abstractfactory.base;
/**
 * 实现B产品接口  创建B具体产品B2
 * @author EminemGolden
 *
 */
public class ProductBImpl2  implements IProductB{

	@Override
	public void createProductB() {
		System.out.println("二号B产品");
	}
}
