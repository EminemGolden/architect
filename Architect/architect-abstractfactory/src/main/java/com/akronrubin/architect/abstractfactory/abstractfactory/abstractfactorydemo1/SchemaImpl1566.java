package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo1;

/**
 * 生产1566型号的电脑主板和CPU
 * @author EminemGolden
 *
 */
public class SchemaImpl1566 implements ISchemaFactory {


	@Override
	public CPUApi createCPU() {
		return new IntelCPU(1566);
	}

	@Override
	public MainboardApi createMainboard() {
		return new MSIMainboard(1566);
	}

}
