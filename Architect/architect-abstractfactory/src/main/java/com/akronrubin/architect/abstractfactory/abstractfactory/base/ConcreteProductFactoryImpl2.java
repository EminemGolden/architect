package com.akronrubin.architect.abstractfactory.abstractfactory.base;
/**
 * 实现生产对应一型产品的工厂
 * 一型产品由产品A2+产品B2 组合创建
 * @author EminemGolden
 *
 */
public class ConcreteProductFactoryImpl2  implements IProductFactory{

	@Override
	public IProductA createProductA() {
		// TODO Auto-generated method stub
		return new ProductAImpl2();
	}

	@Override
	public IProductB createProductB() {
		// TODO Auto-generated method stub
		return new ProductBImpl2();
	}

}
