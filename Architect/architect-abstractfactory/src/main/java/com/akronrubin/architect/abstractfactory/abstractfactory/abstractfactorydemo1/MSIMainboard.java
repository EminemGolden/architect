package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo1;

/**
 * 微星主板
 * 
 */
public class MSIMainboard implements MainboardApi {

	private int cpuHoles;

	public MSIMainboard(int cpuHoles) {
		super();
		this.cpuHoles = cpuHoles;
	}

	@Override
	public void installCPU() {
		System.out.println("微星的主板.cpuHoles:"+cpuHoles);
	}

}
