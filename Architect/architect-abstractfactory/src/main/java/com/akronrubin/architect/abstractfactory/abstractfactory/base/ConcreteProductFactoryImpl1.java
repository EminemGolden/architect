package com.akronrubin.architect.abstractfactory.abstractfactory.base;

/**
 * 实现生产对应一型产品的工厂
 * 一型产品由产品A1+产品B1 组合创建
 * @author EminemGolden
 *
 */
public class ConcreteProductFactoryImpl1 implements IProductFactory {

	@Override
	public IProductA createProductA() {
		// TODO Auto-generated method stub
		return new ProductAImpl1();
	}

	@Override
	public IProductB createProductB() {
		// TODO Auto-generated method stub
		return new ProductBImpl1();
	}

}
