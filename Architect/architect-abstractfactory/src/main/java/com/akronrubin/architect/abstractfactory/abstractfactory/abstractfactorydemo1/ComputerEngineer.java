package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo1;

/**
 * 装机工程师
 * 
 * @author EminemGolden
 *
 */
public class ComputerEngineer {

	/**
	 * 根据客户提供的配件类型组装电脑
	 * 
	 * @param schema
	 */
	public void makeComputer(ISchemaFactory schema) {
		// 1.准备硬件
		prepareHardwares(schema);
		// 2.组装
		// 3.测试
		// 4.交付
	}

	/**
	 * 准备硬件
	 * 
	 * @param schema
	 */
	private void prepareHardwares(ISchemaFactory schema) {
		schema.createCPU().calculate();
		schema.createMainboard().installCPU();
	}

}
