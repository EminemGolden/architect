package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

/**
 * 主板接口
 */
public interface MainboardApi extends Component{

	/**
	 * 安装CPU
	 */
	void installCPU();
	
}
