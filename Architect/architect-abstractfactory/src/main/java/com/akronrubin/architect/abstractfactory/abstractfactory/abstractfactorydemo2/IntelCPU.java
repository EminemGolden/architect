package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

/**
 * 英特尔CPU的实现
 * 
 */
public class IntelCPU implements CPUApi {

	/**
	 * 针脚数目
	 */
	private int pins;

	public IntelCPU(int pins) {
		super();
		this.pins = pins;
	}

	@Override
	public void calculate() {
		System.out.println("因特尔CPU计算.pins:"+pins);
	}

}
