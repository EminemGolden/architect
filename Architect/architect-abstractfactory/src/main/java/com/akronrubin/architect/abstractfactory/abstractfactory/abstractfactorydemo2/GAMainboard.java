package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

/**
 * 技嘉主板
 * 
 */
public class GAMainboard implements MainboardApi {

	/**
	 * CPU 插槽的孔数
	 */
	private int cpuHoles;

	public GAMainboard(int cpuHoles) {
		super();
		this.cpuHoles = cpuHoles;
	}

	@Override
	public void installCPU() {
		System.out.println("技嘉的主板，cpuHoles:"+cpuHoles);
	}

}
