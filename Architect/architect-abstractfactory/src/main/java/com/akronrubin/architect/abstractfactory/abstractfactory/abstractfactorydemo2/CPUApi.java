package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

/**
 * CPU接口
 */
public interface CPUApi extends Component {

	/**
	 * 计算
	 */
	void calculate();
	
}
