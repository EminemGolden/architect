package com.akronrubin.architect.abstractfactory.abstractfactory.simplefactory;

/**
 * AMD CPU实现
 */
public class AMDCpu implements CPUApi {

	private int pins;

	public AMDCpu(int pins) {
		super();
		this.pins = pins;
	}

	@Override
	public void calculate() {
		System.out.println("AMD CPU计算.pins:"+pins);
	}

}
