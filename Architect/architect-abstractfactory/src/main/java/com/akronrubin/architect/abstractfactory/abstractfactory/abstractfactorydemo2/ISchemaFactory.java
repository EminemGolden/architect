package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

/**
 * 定义生成对应组件的产品簇方法
 * 
 * @author EminemGolden
 *
 */
public interface ISchemaFactory {
	Component createProduct(int type);

}
