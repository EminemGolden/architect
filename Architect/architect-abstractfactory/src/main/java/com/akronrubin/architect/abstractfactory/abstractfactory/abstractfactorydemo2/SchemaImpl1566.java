package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

/**
 * 生产1566型号的电脑主板和CPU
 * @author EminemGolden
 *
 */
public class SchemaImpl1566 implements ISchemaFactory {


	@Override
	public Component createProduct(int type) {
		switch (type) {
		case Component.PRODUCT_TYPE_CPU:
			return new IntelCPU(1566);
		case Component.PRODUCT_TYPE_MAINBORD:
			return new MSIMainboard(1566);
		case Component.PRODUCT_TYPE_MEMORY:
			return new KingstonMemory();
		default:
			return null;
		}
	}


}
