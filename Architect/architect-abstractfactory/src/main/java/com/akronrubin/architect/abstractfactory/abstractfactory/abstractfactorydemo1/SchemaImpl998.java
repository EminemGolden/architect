package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo1;

/**
 * 生产998型号的电脑主板和CPU
 * 
 * @author EminemGolden
 *
 */
public class SchemaImpl998 implements ISchemaFactory {

	@Override
	public CPUApi createCPU() {
		return new AMDCpu(998);
	}

	@Override
	public MainboardApi createMainboard() {
		return new GAMainboard(998);
	}

}
