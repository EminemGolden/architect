package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo1;

/**
 * 定义生成对应组件的产品簇方法
 * 
 * @author EminemGolden
 *
 */
public interface ISchemaFactory {

	CPUApi createCPU();

	MainboardApi createMainboard();

}
