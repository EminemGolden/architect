package com.akronrubin.architect.abstractfactory.abstractfactory.base;

/**
 * 定义生成对应组件的产品簇方法
 * 
 * @author EminemGolden
 *
 */
public interface IProductFactory {

	IProductA createProductA();

	IProductB createProductB();

}
