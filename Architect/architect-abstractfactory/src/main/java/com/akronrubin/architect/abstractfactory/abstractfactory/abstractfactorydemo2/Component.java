package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

/**
 * 电脑配件总类
 * @author EminemGolden
 *
 */
public interface Component {

	public static final int PRODUCT_TYPE_CPU = 1;
	public static final int PRODUCT_TYPE_MEMORY = 2;
	public static final int PRODUCT_TYPE_MAINBORD = 3;
	
}
