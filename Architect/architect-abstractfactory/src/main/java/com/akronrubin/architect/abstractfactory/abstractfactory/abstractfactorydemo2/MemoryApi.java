package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

public interface MemoryApi extends Component {
	
	/**
	 * 存储
	 */
	void memorySave();

}
