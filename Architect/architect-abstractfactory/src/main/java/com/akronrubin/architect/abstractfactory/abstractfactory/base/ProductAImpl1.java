package com.akronrubin.architect.abstractfactory.abstractfactory.base;
/**
 * 实现产品接口  创建具体产品A1
 * @author EminemGolden
 *
 */
public class ProductAImpl1 implements IProductA{

	@Override
	public void createProductA() {
		System.out.println("一号A产品");
	}
}
