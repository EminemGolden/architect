package com.akronrubin.architect.abstractfactory.abstractfactory.base;
/**
 * 实现B产品接口  创建B具体产品B1
 * @author EminemGolden
 *
 */
public class ProductBImpl1  implements IProductB{

	@Override
	public void createProductB() {
		System.out.println("一号B产品");
	}

}
