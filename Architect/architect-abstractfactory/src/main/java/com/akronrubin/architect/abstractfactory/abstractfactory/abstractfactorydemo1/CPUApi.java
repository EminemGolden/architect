package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo1;

/**
 * CPU接口
 */
public interface CPUApi {

	/**
	 * 计算
	 */
	void calculate();
	
}
