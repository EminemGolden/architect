package com.akronrubin.architect.abstractfactory.abstractfactory.base;

/**
 * 定义产品接口
 * 
 * @author EminemGolden
 *
 */
public interface IProductA {
	void createProductA();
}
