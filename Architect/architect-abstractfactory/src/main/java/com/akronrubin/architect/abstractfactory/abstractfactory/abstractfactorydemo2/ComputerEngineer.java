package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

/**
 * 装机工程师
 * 
 * @author EminemGolden
 *
 */
public class ComputerEngineer {
	
	private MemoryApi memory;
	private MainboardApi mainboard;
	private CPUApi cpu;

	/**
	 * 根据客户提供的配件类型组装电脑
	 * 
	 * @param schema
	 */
	public void makeComputer(ISchemaFactory schema) {
		// 1.准备硬件
		prepareHardwares(schema);
		// 2.组装
		// 3.测试
		// 4.交付
	}

	/**
	 * 准备硬件
	 * 
	 * @param schema
	 */
	private void prepareHardwares(ISchemaFactory schema) {
		
	cpu = (CPUApi) schema.createProduct(Component.PRODUCT_TYPE_CPU);
	cpu.calculate();
	memory = (MemoryApi) schema.createProduct(Component.PRODUCT_TYPE_MEMORY);
	memory.memorySave();
	mainboard = (MainboardApi) schema.createProduct(Component.PRODUCT_TYPE_MAINBORD);
	mainboard.installCPU();
		
	}

}
