package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo1;

/**
 * 主板接口
 */
public interface MainboardApi {

	/**
	 * 安装CPU
	 */
	void installCPU();
	
}
