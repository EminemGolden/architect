package com.akronrubin.architect.abstractfactory.abstractfactory.abstractfactorydemo2;

/**
 * 生产998型号的电脑主板和CPU
 * 
 * @author EminemGolden
 *
 */
public class SchemaImpl998 implements ISchemaFactory {

	@Override
	public Component createProduct(int type) {
		switch (type) {
		case Component.PRODUCT_TYPE_CPU:
			return new AMDCpu(998);
		case Component.PRODUCT_TYPE_MAINBORD:
			return new GAMainboard(998);
		case Component.PRODUCT_TYPE_MEMORY:
			return new SamsungMemory();
		default:
			return null;
		}
	}

}
