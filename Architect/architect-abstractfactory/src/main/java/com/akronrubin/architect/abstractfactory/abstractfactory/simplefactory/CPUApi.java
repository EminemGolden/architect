package com.akronrubin.architect.abstractfactory.abstractfactory.simplefactory;

/**
 * CPU接口
 */
public interface CPUApi {

	/**
	 * 计算
	 */
	void calculate();
	
}
