package com.akronrubin.architect.abstractfactory.abstractfactory.base;

public interface IProductB {
    
	void createProductB();
	
}
