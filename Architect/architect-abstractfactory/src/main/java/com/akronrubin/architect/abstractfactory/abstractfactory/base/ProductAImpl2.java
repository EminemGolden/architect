package com.akronrubin.architect.abstractfactory.abstractfactory.base;

/**
 * 实现A产品接口 创建具体A产品A2
 * 
 * @author EminemGolden
 *
 */
public class ProductAImpl2 implements IProductA {
	@Override
	public void createProductA() {
		System.out.println("二号A产品");
	}
}
