package com.akronrubin.architect.abstractfactory.abstractfactory.simplefactory;

/**
 * CPU工厂
 */
public class CPUFactory {

	/**
	 * 生成CPU的工厂
	 * @param type 1 生产Intel，2生产AMD
	 * @return
	 */
	public static CPUApi createCPUApi(int type){
		CPUApi cpu = null;
		switch (type) {
		case 1:
			cpu = new IntelCPU(1156);
			break;

		case 2:
			cpu = new AMDCpu(939);
			break;
		default:
			break;
		}
		return cpu;
	}
	
}
