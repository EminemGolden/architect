package com.akronrubin.architect.memento.base;

/**
 * 备忘录管理器
 * 
 * @author wangdenghui
 *
 */
public class Caretaker {

	public void saveMemento(Memento memento) {
		System.out.println("保存状态");
	}

	public Memento retriveMemento() {
		System.out.println("恢复状态");
		return new MementoImple("状态");
	}

}
