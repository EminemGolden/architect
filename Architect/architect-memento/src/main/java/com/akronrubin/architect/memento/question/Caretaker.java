package com.akronrubin.architect.memento.question;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 备忘录管理器
 * 
 * @author wangdenghui
 *
 */
public class Caretaker {

	private static final String CACHE_PATH = "/Users/eminem/Documents/save.text";

	/**
	 * 保存状态
	 * 
	 * @param memento
	 */
	public void save(Memento memento) {
		ObjectOutputStream objectOutputStream = null;
		try {
			objectOutputStream = new ObjectOutputStream(
					new BufferedOutputStream(new FileOutputStream(new File(CACHE_PATH))));
			objectOutputStream.writeObject(memento);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != objectOutputStream) {
				try {
					objectOutputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * 恢复状态
	 * 
	 * @return
	 */
	public Memento retriveMemento() {
		ObjectInputStream ois = null;
		Memento memento = null;
		try {
			ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(CACHE_PATH))));
			memento = (Memento) ois.readObject();
			return memento;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != memento) {
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
