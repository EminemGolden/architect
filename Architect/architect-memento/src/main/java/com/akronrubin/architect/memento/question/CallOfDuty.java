package com.akronrubin.architect.memento.question;

/**
 * 使命召唤
 * 
 * @author wangdenghui
 *
 */
public class CallOfDuty {

	// 关卡
	private int mCheckPoint = 1;
	// 声明值
	private int mLifeValue = 100;
	// 武器
	private String mWeapon = "沙漠之鹰";

	/**
	 * 玩游戏
	 */
	public void play() {
		System.out.println("玩游戏");
		System.out.println(String.format("第%d关", mCheckPoint) + "奋战杀敌中...");
		mLifeValue -= 10;
		System.out.println("进度升级啦!");
		mCheckPoint++;
		System.out.println(String.format("到达第%d关", mCheckPoint));
	}

	/**
	 * 退出游戏
	 */
	public void quit() {
		System.out.println("-------------");
		System.out.println("当前游戏的属性：" + this.toString());
		System.out.println("退出游戏");
		System.out.println("-------------");
	}

	/**
	 * 创建备忘录对象
	 * 
	 * @return
	 */
	public Memento createMemento() {
		Memento memento = new Memento();
		memento.mCheckPoint = mCheckPoint;
		memento.mLifeValue = mLifeValue;
		memento.mWeapon = mWeapon;
		return memento;
	}

	/**
	 * 恢复状态
	 * 
	 * @param memento
	 */
	public void restore(Memento memento) {
		this.mCheckPoint = memento.mCheckPoint;
		this.mLifeValue = memento.mLifeValue;
		this.mWeapon = memento.mWeapon;
	}

	@Override
	public String toString() {
		return "CallOfDuty [mCheckPoint=" + mCheckPoint + ", mLifeValue=" + mLifeValue + ", mWeapon=" + mWeapon + "]";
	}

}
