package com.akronrubin.architect.memento.base;

/**
 * 具体备忘录
 * 
 * @author wangdenghui
 *
 */
public class MementoImple implements Memento {

	private String state;

	public MementoImple(String state) {
		super();
		this.state = state;
	};

	public void save() {
		System.out.println(state);
	}

	public String getState() {
		return state;
	}

}
