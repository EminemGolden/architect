package com.akronrubin.architect.memento.base;

/**
 * 原发器，会使用备忘录保存该对象的状态
 * 
 * @author wangdenghui
 *
 */
public class Originator {

	private String state;

	public Memento createMemento() {
		return new MementoImple(state);
	}

}
