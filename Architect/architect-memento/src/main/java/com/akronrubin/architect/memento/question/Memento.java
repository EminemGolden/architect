package com.akronrubin.architect.memento.question;

import java.io.Serializable;

/**
 * 备忘录
 * 
 * @author wangdenghui
 *
 */
public class Memento implements Serializable {
	private static final long serialVersionUID = 1L;
	// 关卡
	public int mCheckPoint = 1;
	// 声明值
	public int mLifeValue = 100;
	// 武器
	public String mWeapon = "沙漠之鹰";

	@Override
	public String toString() {
		return "Memento [mCheckPoint=" + mCheckPoint + ", mLifeValue=" + mLifeValue + ", mWeapon=" + mWeapon + "]";
	}

}
