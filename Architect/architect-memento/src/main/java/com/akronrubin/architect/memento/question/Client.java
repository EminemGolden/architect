package com.akronrubin.architect.memento.question;
/**
 * 用备忘录模式解决游戏存档问题
 * @author wangdenghui
 *
 */
public class Client {

	@Test
	public void main() {
		// 游戏
		CallOfDuty game = new CallOfDuty();
		// 游戏存档、恢复管理类
		Caretaker caretaker = new Caretaker();
		if (null != caretaker.retriveMemento()) {
			game.restore(caretaker.retriveMemento());
		}
		// 打游戏
		game.play();
		// 存档
		caretaker.save(game.createMemento());
		// 退出游戏
		game.quit();
	}

}
