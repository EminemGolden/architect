package com.akronrubin.architect.memento.base;
import org.junit.Test;
public class Client {

	@Test
	public void main() {
		// 具体业务类
		Originator originator = new Originator();
		// 备忘录管理类
		Caretaker caretaker = new Caretaker();
		// 备忘录管理类实现备忘操作
		caretaker.saveMemento(originator.createMemento());
		// 恢复
		caretaker.retriveMemento();

	}
}
