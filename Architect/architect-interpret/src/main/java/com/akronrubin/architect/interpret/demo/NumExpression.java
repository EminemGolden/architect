package com.akronrubin.architect.interpret.demo;

/**
 * 数字解释器
 * 
 * @author wangdenghui
 *
 */
public class NumExpression extends ArithmeticExpression {

	private int num;

	public NumExpression(int num) {
		super();
		this.num = num;
	}

	/**
	 * 执行方法重写，将字符串解释为数字
	 */
	@Override
	public int interpret() {
		return this.num;
	}

}
