package com.akronrubin.architect.interpret.demo;

public class SubtractionExpression extends OperateorExpression{
	
	public SubtractionExpression(ArithmeticExpression exp1, ArithmeticExpression exp2) {
		super(exp1, exp2);
	}

	@Override
	public int interpret() {
		return exp1.interpret() - exp2.interpret();
	}

}
