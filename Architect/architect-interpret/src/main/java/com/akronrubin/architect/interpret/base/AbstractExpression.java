package com.akronrubin.architect.interpret.base;


public abstract class AbstractExpression {

	/**
	 * 抽象的解析方法
	 * 
	 * @param ctx
	 */
	public abstract void interpret(Context ctx);

}
