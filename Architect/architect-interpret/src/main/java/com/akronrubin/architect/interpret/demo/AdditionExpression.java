package com.akronrubin.architect.interpret.demo;

/**
 *  加号解释器
 * 
 * @author wangdenghui
 *
 */
public class AdditionExpression extends OperateorExpression {
	public AdditionExpression(ArithmeticExpression exp1, ArithmeticExpression exp2) {
		super(exp1, exp2);
	}

	/**
	 * 重载方法，将数字解释器执行加号操作
	 */
	@Override
	public int interpret() {
		return exp1.interpret() + exp2.interpret();
	}

}
