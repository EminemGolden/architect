package com.akronrubin.architect.interpret.demo;

import java.util.Stack;

public class Calculator {

	// 存储操作相关的解释器
	private Stack<ArithmeticExpression> mExpStack = new Stack<ArithmeticExpression>();

	public Calculator(String expression) {
		String[] elements = expression.split(" ");
		ArithmeticExpression exp1, exp2;
		for (int i = 0; i < elements.length; i++) {
			switch (elements[i].charAt(0)) {
			case '+':// 如果是加号，使用数字解释器，将数字存到对象内部
				exp1 = mExpStack.pop();
				exp2 = new NumExpression(Integer.parseInt(elements[++i]));
				mExpStack.push(new AdditionExpression(exp1, exp2));
				break;
			case '-':// 如果是减号，使用数字解释器，将数字存到对象内部
				exp1 = mExpStack.pop();
				exp2 = new NumExpression(Integer.parseInt(elements[++i]));
				mExpStack.push(new SubtractionExpression(exp1, exp2));
				break;
			default:// 数字解释器，将字符串解释为数字，存入栈中
				mExpStack.push(new NumExpression(Integer.parseInt(elements[i])));
				break;
			}
		}
	}

	/**
	 * 执行计算操作，最终利用递归的方式进行计算
	 * 
	 * @return
	 */
	public int interpret() {
		return mExpStack.pop().interpret();
	}

}
