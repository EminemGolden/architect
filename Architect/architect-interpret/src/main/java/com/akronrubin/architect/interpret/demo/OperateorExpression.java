package com.akronrubin.architect.interpret.demo;

public abstract class OperateorExpression extends ArithmeticExpression {

	protected ArithmeticExpression exp1, exp2;

	
	
	public OperateorExpression(ArithmeticExpression exp1, ArithmeticExpression exp2) {
		super();
		this.exp1 = exp1;
		this.exp2 = exp2;
	}

	@Override
	public abstract int interpret();

}
