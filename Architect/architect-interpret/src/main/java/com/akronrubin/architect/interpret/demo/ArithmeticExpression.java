package com.akronrubin.architect.interpret.demo;

/**
 * 抽象的算术运算解释器（表达式）
 * 
 * @author wangdenghui
 *
 */
public abstract class ArithmeticExpression {
	/**
	 * 抽象的解析方法
	 * 
	 * @return 解析得到具体的值
	 */
	public abstract int interpret();

}
